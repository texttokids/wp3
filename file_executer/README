Ce document explique le fonctionnement et l'installation de la partie de traitement.

### Fonctionnement ###

# Dans sa précédente version la chaine fonctionnait de la manière suivante : 

Lorsqu’une demande était faite sur la page web, le serveur web lançait lui-même la requête et attendait une réponse pour continuer son exécution, ce qui posait problème car lors d’un long temps de traitement la page était en chargement constant et pouvait crash.
Les résultats étaient ensuite directement affichés après une redirection vers la page de résultats si la requête de traitement avait pu finir sans problème.


# La nouvelle version fonctionne de la manière suivante : 

=> Vous remplissez un formulaire avec comme choix : 
- Email : champ texte avec vérification JavaScript pour entrer un email au format *****@*****.*** cet email sera utile pour la récupération des résultats et pour être notifié de la disponibilité de ces derniers.

- Institution : champ texte pour indiquer l’institution de l’utilisateur (ne sert qu’à des fins statistiques).

- Niveau de traitement : phrases et/ou texte (même chose que sur l’ancienne version).

- Choix des processeurs : même principe qu’avant mais les processeurs sont juste organisés en catégorie (Il y a juste une petite différence dans le back-end qui est détaillé dans la partie correspondante de ce doc).

- Texte à analyser : à la place d’une zone de texte on a maintenant le choix entre la zone de texte ou un bouton de téléversement de fichiers (possibilité de choisir plusieurs fichiers en même temps).

- Exécuter : lance la requête du formulaire de manière asynchrone vers le serveur web (ne peut être cliqué si l’email n’est pas au bon format).

***
--- Des messages s’affichent en haut du formulaire lorsqu’il y a une erreur (ex : zone de texte vide, aucun choix de fichiers, …) il y a également des messages d’informations lorsqu’un téléversement est fini ou qu’il est encore en cours.

--- Des barres de chargement s’affichent sous le formulaire pour indiquer à l'utilisateur les téléversements en cours (possibilité d’annuler un téléversement avec le bouton d’annulation).

--- Possibilité d’afficher un guide sur l’utilisation à droite du formulaire en cliquant sur le bouton “Afficher/Masquer l’aide”
***

=> Lorsque la page web reçoit la requête en POST elle vérifie si le formulaire est valide et si c’est le cas elle enregistre les fichiers transmis (en écrit un avant dans le cas d’une zone texte) dans une base de donnée (sqlite3 table “texttokids_fichiers”). 
Les champs enregistrés sont : email / institution / nom du fichier / niveau de traitement / processeurs / chemin du fichier / statut (traité ou non) / traitement (chaine ou sémantique) / date de soumission 

=> Le programme Main.py dans file_executer tourne de son côté et vérifie s' il y a des fichiers en attente de traitement dans la base de données. Si c’est le cas, il lance une fonction d'exécution qui va lancer les traitements pour chaque fichier en attente puis enregistrer leurs résultats dans le dossier de sortie ainsi que dans la base de données avec les mêmes champs que pour fichier mais sans le statut. 
Le statut du fichier est passé à True pour indiquer qu’il à fini d’être traité. 
Si le programme n’a aucun fichier à exécuter, il se met en pause pendant un lapse de temps défini avant de revérifier si il y a des fichiers en attente.

=> Pour consulter ses résultats il suffit de se rendre sur la page Résultat du site et d’entrer son email. Un table avec tous les fichiers traités correspondant à l’email vont s’afficher (uniquement les fichiers soumis au cours des x derniers jours). Il y a la possibilité de télécharger chaque fichier individuellement avec le bouton télécharger ou de tout télécharger d’un coup dans un zip.


### Détails technique ###

# Partie web : 

 --- (dans text_complexity_client/django/static/js) ---
=> choix.js : s’occupe des EventListener notamment pour changer l’html au niveau du choix de texte (zone texte / fichier)
=> upload.js : s’occupe de la partie upload avec de l’ajax lorsque le bouton exécuter est cliqué.
Il n’y a pas de balise 'form' sur la page, les données sont récupérées et le script crée un objet formulaire auquel il ajoute les différentes données après avoir effectué des vérifications.
Si toutes les vérifications sont passées et que les données sont ajoutées au formulaire, une requête en XLM est envoyée au serveur (cela permet de ne pas recharger la page de d’envoyer les données en POST en arrière plan). Il y a également une barre de chargement qui est ajoutée à la page pour suivre l’évolution de l’envoie. (cf. upload.js qui contient des commentaires pour retrouver chaque étape)

Pour générer des formulaires, django utilise forms.py et permet une vérification de validité mais pour séparer les processeurs en catégorie il a fallu les diviser en groupe et pour éviter de rendre chaque groupe obligatoire ils sont ajoutés au groupe processeur initiale (ce qui permet de garder l’aspect vérification par django en ayant choisi au moins un processeur de n’importe quelle catégorie) autrement dit le groupe global de processeur existe mais n’est pas affiché et chaque choix d’une sous catégorie de processeur est ajouté au groupe global qui lui est obligatoire plutôt qu'à sa catégorie donc on garde l’obligation de choisir au minimum un processeur pour la validation. (cf. ligne 62-67 upload.js)
Si la requête est bien finie, le serveur renvoie un message de succès qui est affiché par la page, sinon un message d'erreur.
Si l’utilisateur tente de quitter la page alors que des requêtes sont en cours, il aura un message l’avertissant.


 --- (dans text_complexity_client/django/texttokids) ---
***
les répertoires d’enregistrement des fichiers sont récupérés dans le fichier settings.config de file_executer au début des fichiers suivant (si besoin)
***

=> forms.py : gère les formulaires
=> models.py : gère les tables de la base de données
=> views.py : différentes pages ou fonction du site : 
- chaine : Affiche le template de la page “chaine” en lui fournissant les données correspondante (création du formulaire). Si elle reçoit des données en POST, elle vérifie la validité du formulaire et si le formulaire et valide crée un objet de la base de données avec les informations extraites du formulaire. (stocke le mail et institution en session pour les pré remplir).

- semantics : idem que chaine mais avec les données correspondant à sémantique

- results : Récupère les données correspondant au mail entré dans la table résultats et les transmet au template results pour produire l’affichage résultant (soit pas de fichier soit la table avec les infos récupérées de la base de données).

- download : lorsque l’utilisateur clique sur le bouton télécharger dans la table de résultat cela appelle cette fonction en lui fournissant l’id du fichier pour que la fonction le récupère dans la base de données et lance le téléchargement.

- download_all : similaire à download mais récupère tous les fichiers avec le mail et les zip à l’aide du module python ZipFile en créant aussi un fichier infos avec les informations de chaque fichier contenu dans le zip.

* Pour les téléchargement les id ou mail sont transmis via des hyperlien faisant référence aux fonctions correspondantes (voir dans urls.py)

# Programme d'exécution (dossier file_executer):

!!! Le répertoire file_executer doit se trouver dans le même répertoire que text_complexity_client et text_complexity_server !!!

=> settings.config : 
- inputs : chemin absolu pour le répertoire d’entrée des fichiers à traiter
- outputs : chemin absolu pour le répertoire de sortie des résultats
- sqlite3_db_path : chemin relatif vers le fichier de la base de données (Ne pas changer !)

=> Main.py : 
Lorsqu’on lance le programme dans un terminal (python Main.py) : 
- check de l’existence des répertoires rentrés dans settings.config (le programme s’arrête si introuvable)
- une boucle infinie est lancée qui check si il y a des fichiers en attente (si oui lance launch_chain(), sinon sleep())

*** Fonction principale : launch_chain() ***
-> Cette fonction va s’occuper de récupérer les fichiers en attente et de lancer une boucle pour traiter chaque fichier l’un après l’autre. (plusieurs try/except sont présents pour permettre de continuer l'exécution de la boucle si il y a un problème lors du traitement d’un fichier, donc si un fichier échoue, les prochains seront quand même traités).
-> Selon le type de fichier et les traitements demandés par l’utilisateur différents traitements vont être lancés avec les fonctions qui suivent.
-> Si le traitement est un succès, le statut du fichier et mis à jour et le résultat est enregistré dans la base de données (si new_name existe c’est que les fonctions de traitement ont réussi et renvoyé un résultat donc c’est une condition pour tester si tout c’est bien passé avant de changer le statut et mettre à jour la base de données).

*** gestion base de donnée : file_waiting() ***
-> Compte le nombre de fichiers au statut False dans la table texttokids_fichiers.
-> Retourne True si le nombre est supérieur à 0.

*** Gestion base de donnée : list_files() ***
-> Récupère et retourne un tuple de tuple qui correspondent à chaque ligne avec un statut False dans texttokids_fichiers.

*** Gestion base de donnée : update_statut(id) ***
-> Passe le statut à True dans la base de données de la ligne d’id {id}.

*** Gestion base de donnée : delete_file(path, id) ***
-> Supprime le fichier au chemin {path} fourni en paramètre.
-> Supprime de la base de données la ligne d’id {id} fourni en paramètre.

*** Gestion base de donnée : insert_results(email, name, levels, processors, fichier, traitement, date) ***
-> Insert dans la base de données (texttokids_resultats) les données fournies en paramètre.
-> Retourne True si l’insertion à réussi.

!!! A noter que lorsque launch_chain exécute un traitement sur un zip, le répertoire de travail change pour file_executer/zip_temporary/ !!!

*** Gestion des zip : read_zip(filename) ***
-> Extrait le zip au chemin {filename} dans le répertoire file_executer/zip_temporary/ 
-> Parcours ce répertoire pour ajouter tous les chemins des fichiers présents dans une liste.
-> Retourne la liste des chemins.

*** Gestion des zip : execute_zip(file_paths, choice_levels, choice_processors, traitement) ***
-> Pour tous les fichiers dont les chemins sont dans la liste {file_path} va exécuter un traitement avec l’appel des fonctions de traitement en leur fournissant leur reste des paramètres.
-> Chaque nouveau fichier créé par la fonction de traitement est enregistré au même chemin et le chemin de ces fichiers résultats est stocké dans une liste.
-> Retourne la liste des chemins de fichiers résultats.

*** Gestion des zip : write_zip(name, file_to_zip) ***
-> Écrit {name}.zip qui contient tous les fichiers aux chemins de la liste {file_to_zip}.
-> Retourne name (pour le nouveau nom).

!!! Une fois le traitement zip terminé le contenu de zip_temporary est supprimé et le répertoire de travail du programme revient à celui de Main.py !!!

*** Gestion du traitement : read_file(file_path) ***
-> La fonction ouvre le fichier et essaye de le lire en UTF-8 pour stocker son contenu dans une variable choice_text.
-> Si le contenu ne peut pas être lu en UTF-8 la variable choice_text prendra la valeur 0.
-> Retourne la variable choice_text.

*** Gestion du traitement : chaine(filename, choice_text, choice_levels, choice_processors, tmp_data = './data', out_path = output_dir) ***
-> La fonction envoie les données de choice_text, choice_levels et choice_processors au webservice pour exécuter le traitement.
-> Une fois la réponse du webservice obtenue, le fichier est enregistré dans le répertoire out_path au format xlsx.
-> Pour la transformation en xlsx, le programme les stocke d’abord en csv qui sont stockés dans tmp_data puis supprimé une fois le tout fusionné en un fichier de nom {filename}.xlsx .
-> Les deux variables prédéfinis (tmp_data et out_path) servent pour être changées lors de l’exécution zip du fait que les répertoire de travail et stockage changent.
-> Retourne le nouveau nom du fichier

*** Gestion du traitement : (filename, choice_levels, choice_processors, f_stockage = input_dir, f_result = output_dir, alt_path = "../text_complexity_server/alternative/rashedur/") ***
-> Principe similaire à chaine mais cette fois exécute la fonction alternative.
-> Les variables prédéfinies servent également pour être changées lors du traitement zip.
-> Retourne le nouveau nom du fichier sous la forme {filename}.zip .


### Installation ###

!!! Attention vous devez avoir python d’installé et suivi l’installation de text_complexity_server pour avoir les librairies nécessaire à l’exécution de certaines parties !!!

Pour l'installation, il vous faut modifier le fichier 'settings.config' dans le répertoire ‘file_executer’ pour y mettre les chemins de vos répertoires d’entrée et de sortie (vous pouvez utiliser ceux de file_executer : inputs/outputs )
-> modifier inputs et outputs dans le fichier de config.

Il vous faut un répertoire qui contient les trois répertoires suivants : text_complexity_client / text_complexity_server / file_executer.

A l’aide d’un terminal, lancez l’environnement conda que vous aviez créé (cf. installation text_complexity_server) et lancez Main.py (python Main.py).
Le programme devrait se lancer et il suffit de le laisser tourner en fond lorsque vous utilisez l’interface web (en local) pour uploader vos fichiers.



