from tkinter import *
from t2k_exec import main
from multiprocessing import Process, Queue
import sys

def lancement():
    global action_process
    if not action_process.is_alive():
        action_process = Process(target=main, args=(q,))
        action_process.start()
        window.print_in_app()

def stopping():
    if action_process.is_alive():
        action_process.terminate()
        print("fin")

class Redirect():
    
    def __init__(self, widget):
        self.widget = widget

    def write(self, text):
        self.widget.insert('end', text)
        self.widget.see('end')


class MyWindow(Tk):
    def __init__(self):
        Tk.__init__(self)

        self.geometry('1000x600')
        self.rowconfigure(2, weight=1)
        self.columnconfigure(1, weight=1)
        self.title("Interface d'utilisation local T2K")
        self.page_terminal()
        #self.after(5000, self.print_in_app)
    
    def page_terminal(self):
        f = Frame(self, bg='#777777',borderwidth = 1)
        f.grid(row=2,column=1,sticky='NSWE', padx=5, pady=5)

        button = Button(self, text="stop", command=stopping)
        button.grid(row=0,column=0,sticky=NW)

        button = Button(self, text="launch", command=lancement)
        button.grid(row=1,column=0,sticky=NW)

        scrollbar = Scrollbar(f)
        scrollbar.pack(side = RIGHT,fill=Y)

        self.mylist = Listbox(f, yscrollcommand = scrollbar.set )

        self.mylist.pack(fill=BOTH,expand=True)
        scrollbar.config(command = self.mylist.yview)

        button = Button(self, text="test", command=self.print_in_app)
        button.grid(row=2,column=0,sticky=NW)
    
    def print_in_app(self):
        global q
        while not q.empty():
            self.mylist.insert(END,q.get_nowait())
            self.mylist.see("end")
        self.after(5000, self.print_in_app)


if __name__ == '__main__':
    q = Queue()
    action_process = Process(target=main, args=(q,))
    window = MyWindow()
    window.mainloop()




