import sqlite3
import configparser
import sys

config = configparser.ConfigParser()
config.read('settings.config')
db_path = config['database']['sqlite3_db_path']
db_in = config['database']['db_uploaded_files']
db_out = config['database']['bd_results_files']


def file_waiting():
    # print("file waiting")
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    qry="SELECT count(*) FROM " + db_in + " WHERE statut=False"
    try:
        cur.execute(qry)
        r = cur.fetchone()[0]
    except Exception as e:
        print (e)
        print ("Error: unable to fecth data")
        r = 0
    conn.close()
    # print("SQLITE data was measured", r)
    return r!=0

def update_statut(id, choice_db = 'fichier'):
    conn=sqlite3.connect(db_path)
    cur=conn.cursor()
    if choice_db == 'fichier':
        qry="UPDATE " + db_in + " SET statut = True WHERE id=" + str(id)
    else :
        qry="UPDATE " + db_out + " SET statut = True WHERE email='" + id + "'"
    try:
        cur.execute(qry)
        conn.commit()
        print ("\t***** Statut updated *****")
    except Exception as e:
        print(e)
        print ("Error: unable to update data")
    conn.close()

def insert_results(email, name, levels, processors, fichier, traitement, date, date_fin):
    conn=sqlite3.connect(db_path)
    cur=conn.cursor()
    values = "('" + email + "','" + name + "','" + levels + "','" + processors + "','" + fichier + "','" + traitement + "','" + date + "','" + date_fin + "', false)"
    qry="INSERT INTO texttokids_resultats(email, name, levels, processors, fichier, traitement, date, date_fin, statut) VALUES " + values
    try:
        cur.execute(qry)
        conn.commit()
        print ('\t***** Saved in database *****')
        save = True
    except Exception as e:
        conn.rollback()
        print(e)
        print ('Error: INSERT operation')
        save = False
    conn.close()
    return save

def insert_logs(email, error, fichier, date):
    conn=sqlite3.connect(db_path)
    cur=conn.cursor()
    error = error.replace("\"","").replace("'","")
    values = "('" + email + "','" + error + "','" + fichier + "','" + str(date) + "')"
    qry="INSERT INTO texttokids_logerror(email, error, fichier, date) VALUES " + values
    try:
        cur.execute(qry)
        conn.commit()
        print ('\t***** Saved in database *****')
        save = True
    except Exception as e:
        conn.rollback()
        exception_type, exception_object, exception_traceback = sys.exc_info()
        line_number = exception_traceback.tb_lineno
        exception_log = "line " + str(line_number) + " : " + str(exception_type) + " " + str(e)
        print(exception_log)
        print ('Error: INSERT operation')
        save = False
    conn.close()
    return save

def list_files():
    # print("list files")
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    qry="SELECT * FROM " + db_in + " WHERE statut=False"
    try:
        cur.execute(qry)
        ls_files_in_db = cur.fetchall()
    except Exception as e:
        print (e)
        print ("Error: unable to fetch data")
        ls_files_in_db = None
    conn.close()
    # print("SQLITE data was fetched")
    return ls_files_in_db

def delete_file(path, id):
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    qry="DELETE FROM " + db_in + " WHERE id=" + str(id)
    try:
        cur.execute(qry)
        conn.commit()
        print ('\t***** Delete from database *****')
    except Exception as e:
        print (e)
        print ("Error: unable to delete row")
    conn.close()
    os.remove(path)

def get_mail_to_send():
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    qry="SELECT email FROM " + db_out + " WHERE statut=False GROUP BY email"
    try:
        cur.execute(qry)
        ls_mail = cur.fetchall()
    except Exception as e:
        print (e)
        print ("Error: unable to fecth data")
        ls_mail = None
    conn.close()
    return ls_mail