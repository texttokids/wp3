# import sqlite3
import time
import requests
import json
import gc
import sys
import os
import glob
import csv
import shutil
import configparser
import builtins
from datetime import datetime, date, timedelta
from zipfile import ZipFile

from database_management import *
from mail_sender import send_mail

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

config = configparser.ConfigParser()
config.read('settings.config')
input_dir = config['file_management']['inputs']
output_dir = config['file_management']['outputs']
sleep_time = int(config['validation']['sleep_time'])
try_after_error = 'True' == config['validation']['keep_trying']
server_address = config['webservice']['server_address']

sys.path.insert(1, "../text_complexity_server/alternative/rashedur/")
from text_processor import alternative

sys.path.insert(1, "../text_complexity_client/django/texttokids/")
import writecsv


##############################
###                        ###
###     ZIP MANAGEMENT     ###
###                        ###
##############################

def read_zip(filename):
    with ZipFile(filename, 'r') as archive:
        archive.extractall()
    file_paths = []
    for root, directories, files in os.walk("."):
        for inzip_filename in files:
            filepath = os.path.join(root, inzip_filename)
            file_paths.append(filepath)
    return file_paths


def execute_zip(file_paths, choice_levels, choice_processors, traitement):
    res_files_path = []
    count = 0
    total = len(file_paths)
    for f_path in file_paths:
        count += 1
        print("\t\t*****", count, "/", total, "*****")
        if os.path.splitext(f_path)[1] == ".txt":
            dir_file = os.path.split(f_path)
            d_name = dir_file[0] + "/"
            f_name = dir_file[1]
            choice_text = read_file(f_path)
            if choice_text != 0:
                try:
                    if traitement == "analyse globale":
                        new_name = chaine(f_name, choice_text, choice_levels, choice_processors, '../data', d_name)
                        res_files_path.append(d_name + new_name)
                    else:
                        d_name_alt = d_name.replace(".", "../../../file_executer/zip_temporary")
                        new_name = semantique(f_name, choice_levels, choice_processors, d_name_alt, d_name_alt,
                                              "../../text_complexity_server/alternative/rashedur/")
                        res_files_path.append(d_name + new_name)
                except Exception as e:
                    print(e)
                    os.chdir(current_dir + "/zip_temporary")
                    with open(f_path, 'w', encoding='utf-8') as myfile:
                        myfile.write("Error :", e)
                    res_files_path.append(f_path)
                gc.collect()
    return res_files_path


def write_zip(name, files_to_zip):
    with ZipFile(output_dir + name, 'w') as archive:
        for file in files_to_zip:
            archive.write(file)
    return name


################################
###                          ###
###     CHAIN MANAGEMENT     ###
###                          ###
################################

def csv_log_writer(row):
    fields = ["Mail", "Institution", "Nom", "Traitement", "Levels", "Processeurs", "Date début", "Date fin"]
    global last_log_file
    if last_log_file == '' or date.today() - timedelta(days=7) > date.fromtimestamp(os.path.getctime(last_log_file)):
        print("OK")
        last_log_file = './logs/' + str(date.today()) + '.csv'
        with open(last_log_file, 'w', newline='') as csvfile:
            csv.writer(csvfile, delimiter=';').writerow(fields)
    with open(last_log_file, 'a', newline='') as csvfile:
        csv.writer(csvfile, delimiter=';').writerow(row)


def read_file(filepath):
    try:
        with open(filepath, 'r', encoding='utf-8') as myfile:
            choice_text = myfile.read()
    except Exception as e:
        print(e)
        try:
            print("\t***** Tentative UTF-16 *****")
            with open(filepath, 'r', encoding='utf-16') as myfile:
                choice_text = myfile.read()
            with open(filepath, 'w', encoding='utf-8') as myfile:
                myfile.write(choice_text)
        except Exception as e:
            print(e)
            choice_text = 0
            print("\t***** Erreur d'encodage *****")
    return choice_text


def set_local_path():
    localcurdir = os.getcwd()
    print("testchain.py (text_complexity_server/src)", localcurdir)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("this file", dir_path)
    os.chdir(dir_path)
    # sys.path.insert(1, dir_path)
    return localcurdir


def chaine(filename, choice_text, choice_levels, choice_processors, tmp_data='./data', out_path=output_dir):

    # direct call version
    print("\t***** Appel direct de requête *****")

    locpth = set_local_path()
    os.chdir("../text_complexity_server/src/")
    # sys.path.insert(1, "../text_complexity_server/src/")
    sys.path.insert(1, "./")
    from api.descriptors_extractor.extraction_ws import process_chain
    print("**** PASSED import ****")
    data_json = process_chain(choice_text, choice_levels, choice_processors)
    # json_readable = json.dumps(data_json, indent=4, ensure_ascii=False)
    os.chdir(locpth)
    print("\t***** Ecriture du fichier xlsx *****")

    # webservice version
    # print("\t***** Requête envoyée au webservice *****")
    # data = {'text': choice_text, 'processors': choice_processors, 'levels': choice_levels}
    #
    # response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)
    #
    # data_json = response.json()
    # json_readable = json.dumps(data_json, indent=4, ensure_ascii=False)
    #
    # print("\t***** Ecriture du fichier xlsx *****")

    new_name = filename.replace(".txt", ".xlsx")
    list_files = writecsv.write_analysis_result(tmp_data, 0, 'textfield', data_json)
    writecsv.write_xlsx_file(out_path + new_name, list_files)

    for tfn in list_files:
        os.remove(tfn)
        print("remove: " + tfn)

    # print("\t***** Ecriture du fichier json *****")
    # with open(out_path + new_name, "w", encoding='utf-8') as resFile:
    #     resFile.write(json_readable)
    #     resFile.close()

    return new_name


def semantique(filename, choice_levels, choice_processors, f_stockage=input_dir, f_result=output_dir,
               alt_path="../text_complexity_server/alternative/rashedur/"):
    print("\t***** Execution d'alternative *****")
    new_name = filename.replace(".txt", ".zip")
    alternative(alt_path, f_stockage, f_result, filename, new_name, choice_levels, choice_processors)
    return new_name


def launch_chain():
    ls_files = list_files()
    count = 0
    nb_file = len(ls_files)

    for current_file in ls_files:
        count += 1
        try:
            # Data extraction from database records
            id_db = current_file[0]
            save_mail = current_file[1]
            choice_levels = list(current_file[3].split(","))
            choice_processors = list(current_file[4].split(","))
            file_date = current_file[5]
            type_traitement = current_file[7]
            choice_institution = current_file[8]
            name = current_file[9]
            new_name = None

            print("*****", count, "/", nb_file, "*****")
            print("***** Traitement de :", name, "*****\n")

            file_format = os.path.splitext(name)[1]

            if file_format == ".zip":
                prev_dir = os.getcwd()
                os.chdir("./zip_temporary")
                for dir in os.listdir():
                    try:
                        shutil.rmtree('./' + dir)
                    except Exception as e:
                        print(e)
                        print("Unable to remove :", dir)
                try:
                    print("\t***** Zip extraction *****")
                    file_paths = read_zip(input_dir + name)
                    print("\t***** Zip execution *****")
                    res_files_path = execute_zip(file_paths, choice_levels, choice_processors, type_traitement)
                    print("\t***** Zip compression *****")
                    new_name = write_zip(name, res_files_path)
                    print("\t***** Remove zip temporary files *****")
                except Exception as e:
                    exception_type, exception_object, exception_traceback = sys.exc_info()
                    file_name = exception_traceback.tb_frame.f_code.co_filename
                    line_number = exception_traceback.tb_lineno
                    exception_log = str(file_name) + " line " + str(line_number) + " : " + str(
                        exception_type) + " " + str(e)
                    print("\t***** Error in zip treatment *****")
                    print(exception_log)
                    os.chdir(prev_dir)
                    if insert_logs(save_mail, exception_log, name, datetime.now()) and not try_after_error:
                        update_statut(id_db)

                os.chdir(prev_dir)

            elif file_format == ".txt":
                choice_text = read_file(input_dir + name)
                if choice_text == 0:
                    new_name = "error_" + name
                    with open(output_dir + new_name, "w", encoding='utf-8') as errorFile:
                        errorFile.write("Erreur d'encodage")
                else:
                    if type_traitement == "analyse globale":
                        new_name = chaine(name, choice_text, choice_levels, choice_processors)
                    else:
                        print(name)
                        print(choice_levels)
                        print(choice_processors)
                        new_name = semantique(name, choice_levels, choice_processors)

            else:
                print("\t***** Wrong file extension *****")
                delete_file(input_dir + name, id_db)

            date_fin = str(datetime.now())

            if new_name != None and insert_results(save_mail, new_name, current_file[3], current_file[4], new_name,
                                                   type_traitement, file_date, date_fin):
                update_statut(id_db)
                csv_log_writer(
                    [save_mail, choice_institution, new_name, type_traitement, current_file[3], current_file[4],
                     file_date, date_fin])

        except Exception as e:
            exception_type, exception_object, exception_traceback = sys.exc_info()
            file_name = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            exception_log = str(file_name) + " line " + str(line_number) + " : " + str(exception_type) + " " + str(e)
            print("***** !!! SOMETHING WENT WRONG !!! *****")
            import traceback
            print(traceback.format_exc())
            print(exception_log)
            os.chdir(current_dir)
            if insert_logs(save_mail, exception_log, name, datetime.now()) and not try_after_error:
                update_statut(id_db)
            gc.collect()


def main(o=None):
    global last_log_file
    last_log_list = glob.glob('./logs/' + '*.csv')
    if len(last_log_list) == 0:
        last_log_file = ''
    else:
        last_log_file = max(last_log_list, key=os.path.getctime)
    global current_dir
    current_dir = os.getcwd()

    def print_app(*args):
        string = ''
        for a in args:
            if type(a) != str:
                a = ' ' + str(a)
            string += a
        o.put(string)

    if o != None:
        global print
        print = print_app
    else:
        print = builtins.print
    if not os.path.exists(input_dir) or not os.path.exists(output_dir):
        print("Erreur sur les chemins des repertoires dans settings.config!")
        exit()
    while True:
        if file_waiting():
            print("\n***** Files waiting *****")
            print("***** Launching chain *****\n")
            launch_chain()
            print("***** Sending emails *****")
            for current_email in get_mail_to_send():
                send_mail(current_email[0])
                update_statut(current_email[0], 'resultat')
            print("***** End chain *****")
        print("***** Sleeping for", sleep_time, "secondes *****")
        print("Garbage Collector :", gc.collect())  # Garbage Collector pour libérer la mémoire pendant le sleep

        time.sleep(sleep_time)


if __name__ == '__main__':
    main()
