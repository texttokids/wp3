import smtplib
from email.message import EmailMessage


def send_mail(receiver):
    from mailjet_rest import Client
    import os
    # api_key = os.environ['MJ_APIKEY_PUBLIC']
    # api_secret = os.environ['MJ_APIKEY_PRIVATE']
    api_key = '0369c5350e4267ea0a73158b602131bf'
    api_secret = '6818e6f062fb916d29d830658c634aff'

    message_text = """
    Bonjour,

    Certains de vos fichiers ont fini leur traitement et sont disponibles à cette adresse :
    https://texttokids.ortolang.fr/results/

    L'équipe TextToKids !"""

    message_html = """
    <h3>Bonjour,</h3>
    <br/>
    Certains de vos fichiers ont fini leur traitement et sont disponibles à cette adresse :
    <a href=\"https://texttokids.ortolang.fr/results/\">https://texttokids.ortolang.fr/results/</a>
    <br/>
    <h3>L'équipe TextToKids !</h3>"""

    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "christophe.parisse@inserm.fr",
                    "Name": "Christophe Parisse"
                },
                "To": [
                    {
                        "Email": receiver,
                        "Name": receiver
                    }
                ],
                "Subject": "Texttokids: fichiers disponibles!",
                "TextPart": message_text,
                "HTMLPart": message_html,
            }
        ]
    }
    result = mailjet.send.create(data=data)
    if result.status_code != 200:
        print(result.json())

# msg=EmailMessage()
# msg['Subject'] = 'TextToKids fichiers disponibles'
# msg['From'] = 'TextToKids@t2k.com'
# msg['To'] = receiver
# msg.set_content(message)
#
# server_ip = 'localhost'
# server_port = 1025
#
# try:
#     with smtplib.SMTP(server_ip, server_port) as server:
#         server.send_message(msg)
# except Exception as e:
#     print(e)
#     print("Error in sending email")
