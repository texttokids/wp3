aujourd'hui on est le quatre mars deux mille dix onze ..
alors qu'est ce tu as fait aujourd'hui ?.
avant de jouer aux petits chevaux ..
et tu gagnes aux petits chevaux ou pas ?.
tu gagnes tout le temps ?.
non !.
ah des fois ..
et aujourd'hui tu gagnes ?.
ah tu as pas encore joué ..
alors qu'est ce tu as fait avant ?.
tu étais à l'école ?.
c'était vraiment l'école aujourd'hui cet après midi ?.
qu'est ce tu faisais avec maman cet après midi ?.
wah@i les tableaux de Mondrian !.
et c'était bien ?.
ça a duré longtemps ?.
tu t'es un peu ennuyé alors ?.
tu as été gentil ?.
comme une image ..
comme une image ..
et alors qu'est ce tu as //?.
est ce que y a des choses que tu as aimé dans les tableaux de Mondrian ?.
c'est de quelle couleur ?.
qu'est ce que tu aimes pas dans les tableaux de Mondrian ?.
l'arbre noir tu as pas trop aimé ..
y avait que du gris et du noir ..
ah !.
et alors lesquels que tu as aimé ?.
ah !.
y avait pas de vert ?.
ah ..
ah toi tu veux qu'on /..
l'arbre bleu il est vert !.
ben !.
il est bleu alors ?.
où il est vert ?.
ah oui ..
mais le tableau s'appelle l'arbre bleu ?.
c'est rigolo comme nom ça ..
c'est bizarre ça !.
c'est bien mon coeur ..
ben parce que tu as retenu en effet y avait une peinture de Mondrian qui s'appelait l'arbre bleu ....
enfin l'arbre gris je sais plus non mais y avait l'arbre ..
oui bleu ..
il a il a pas aimé l'arbre noir il m'a dit ..
il a /..
boh oui c'est vrai qu'il était un petit peu euh ....
parce que c'était triste ..
et est ce que ça alors je vais te montrer quelque chose ..
mince où est ce que j'ai mis mon sac ?.
je l'ai caché sous le manteau ..
j'ai caché le sac !.
xx ..
mais tu étais tout seul avec maman où y avait d'autres enfants ?.
cet après midi ?.
est ce que ça ça te rappelle quelque chose ?.
xx ..
montre moi bouge pas que je filme !.
d'accord ..
et ça ?.
ah dis donc !.
et alors ?.
et ça ?.
comment il s'appelle ce tableau là ?.
c'est ça ..
ben ça on sait plus ..
tu sais plus toi ?.
tu sais ....
regarde voilà !.
c'est ça en fait voilà montre à tonton tu lui expliques en fait ce que tu as ce qu'on a vu ..
c'est avec l'école hein ..
attends !.
Myriam elle elle avait préparé ....
est ce que tu te souviens de ce tableau là ?.
oui ..
oui tout à fait ..
oui oui c'est cela ..
oui ben tu vois en fait il a bien mmh ..
et ....
et ça je me souviens plus trop mais tu vois c'est ça ça ressemble à ça ..
là ah là c'est Emmanuel qui m'a fait une photocopie donc c'est normal qu'y ait pas de couleur ..
d'accord ?.
parce qu'elle avait pas de photocopie couleur mais normalement y a des couleurs comme ça rouge jaune ....
et ça je viens d'aller chez mon esthéticienne ..
et elle vient de me dire euh ....
euh donc euh Antoine Antoine lui dit ..
elle fait oh tiens Mondrian !.
elle voulait une carte de visite parce que j'ai une euh une cliente qui a un café enfin qui a /..
la gommette ?.
une pochette ?.
ah oui oui ben je l'ai retirée oui parce qu'on avait une petite euh un autocollant et il était de quelle couleur ?.
ah !.
un badge ..
rouge rouge voilà ..
je l'ai retiré oui ah oui ..
ben parce que là ça y est c'est fini je suis plus dans le musée ..
et euh attends ....
euh ?.
c'était à Milo hein oui ?.
pourquoi vous aviez chacun un tableau ?.
xxx ..
elle avait super bien fait ça ..
elle a elle a deux /..
oui ça c'était ....
ça c'était à qui ?.
à Denis ah oui c'était peut être à Denis oui tu as raison ..
et ça qu'est ce qu'elle t'as qu'est ce qu'elle a dit ?.
voilà tout à fait ..
et qu'est ce qu'elle a dit en fait ?.
que ça le nom de ce tableau ?.
comment ça s'appelait ?.
elle a même demandé à Tess si elle connaissait la ville ..
ouais ..
oui pardon ..
Tessa excuse moi ..
tu te souviens du nom de la ville ?.
New ....
New ....
New ....
New York ..
New York ....
alors ce tableau là s'appelle New York city ..
parce que Mondrian il a vécu à Paris ..
mais il a vécu aussi à ....
ben New York ..
et tu sais où c'est New York ?.
on a dit en fait c'est dans quel pays New York ?.
si si rapelle toi ..
si hé les le papa et la maman de Tessa !.
ils habitaient où avant d'aller de venir à Paris ?.
en Amérique aux Etats Unis !.
faudra que je te montre où c'est les Etats Unis mon amour ?.
tu veux que je te montre dans un sur un atlas ..
et Mondrian en fait ....
il a vécu au Etats Unis et il a peint ce tableau ..
et aux Etats Unis qu'est ce qu'il y a ?.
les rues elles sont comment ?.
elles sont comme ça ..
horizontales ..
enfin linéaires comme ça tu vois ..
et il a construit comme ça ..
et qu'est ce qu'elle a dit aussi que les immeubles ils étaient comment aux Etats Unis ?.
à New York ?.
tu as pas écouté là alors Manu Emmanuelle ..
qu'ils étaient très très très grands ..
et ces immeubles là on les appelle comment aussi ?.
des ?.
il sait plus ..
grattes ....
des grattes ....
des grattes ciel ..
et pourquoi on appelle les grattes ciel ?.
comment qu'elle a /..
comment elle a expliqué ?.
parce que ils sont aussi hauts et parce que ils grattent ....
voilà c'est pour cela qu'on appelle le gratte les grattes ciels ..
et pourquoi on les appelle pas grattes nuages ?.
ah ouais ..
ah là si mamie elle avait été là !.
je te dis pas elle serait ravie hein ..
ah parce que mamie elle aime beaucoup l'art ..
les peintures et tout ..
oui ben oui oui je veux juste te montrer où sont les Etats Unis ..
tu m'as demandé ?.
donc je te montre ..
on est d'accord ?.
alors voilà ..
ça c'est le monde !.
d'accord ?.
est ce qu'il y a des couleurs un peu là ou pas ?.
ouais !.
de façon ....
euh ....
ouais non non ..
la mer c'est ce qui est dans cette couleur là ..
ah enfin bon ..
là regarde là y a du bleu c'est plus facile ..
voilà ..
la mer c'est le bleu ..
attends ..
voilà on va faire comme ça ..
regarde mon amour ..
tout ce qui est bleu c'est la mer ..
d'accord ?.
alors nous on habite en France ..
et la France tu as vu ?.
c'est là !.
ok ?.
et là où il y a le point le point rouge là ..
là c'est Paris plus ou moins ..
han !.
c'est tout petit ..
voilà ..
d'accord et là c'est la France ..
ok les Etats Unis c'est beaucoup plus grand les Etats Unis ..
les Etats Unis ça fait tout ça !.
d'accord ?.
et New York ?.
New York on va dire que c'est là ..
et parrain !.
alors nous on est là la France ..
parrain il habite où ?.
bravo et c'est ....
c'est très bien xx ..
super en plus tu tombes pile poil sur le point de Sydney ..
ben là tu m'épates mon coeur ..
et c'est quel pays ?.
c'est quel pays là Sydney ?.
l'Australie ..
l'Australie tu le sais mon chéri ..
super ..
mais attends ça par contre ça je le garde ..
ok ..
c'est loin hein !.
tout petit petit petit ..
fais voir tiens ..
on remontrera ça ..
tu tu veux oui ben je sais oui je vais pas t'embêter avec ça ça sert à ....
allez ..
c'est déjà très bien ..
maintenant il veut jouer !.
voilà on va jouer ..
alors faut que tu m'expliques comment on joue parce que moi je connais pas ..
tu m'expliques hein ?.
je regarde ..
ben oui mais si tu m'expliques pas je vais pas comprendre ..
explique les règles ..
c'est quoi d'abord ..
pour commencer il faut /..
ce que tu as dans les mains c'est quoi ça ?.
et ça sert à quoi ?.
chaque fois qu'on fait un sept on recommence ?.
ah un six !.
et si on fait les autres chiffres qu'est ce qu'on fait ?.
ah ça c'est intéressant !.
d'accord ..
oui pour sortir oui ..
tu veux un café ?.
je veux bien volontiers ..
hein parce que j'ai ....
Antoine tu veux un petit verre d'eau ?.
un petit verre de grenadine tu peux si tu veux ..
xxx ..
alors qu'est ce qui faut faire ?.
pour partir ça c'est quoi ?.
faut un six ..
et ici ça c'est quoi ?.
c'est l'écurie ?.
c'est leur chambre ?.
ça c'est quoi ?.
c'est l'écurie c'est leur chambre ?.
c'est leur chambre ?.
et est ce qu'ils ont la télévision ?.
est ce qu'ils ont la télévision les les chevaux dans leur chambre ?.
est ce qu'ils ont la télévision les chevaux dans leur chambre ?.
qu'est ce qu'ils ont dans leur chambre les chevaux ?.
ah juste là !.
et le reste c'est quoi alors ?.
et le reste alors c'est quoi s'ils habitent que jusque là ..
et le vert il habite où ?.
et les verts ils habitent où ?.
est ce que tu sais combien de temps il faut d'avion pour aller en Australie ?.
cinq fois quoi ?.
l'Australie il faut //..
boum tombé par terre ..
ben ça sert à rien de le lancer loin pour faire un un ..
ben ..
moi je pensais que si tu l'avais lancé loin tu aurais fait au moins un six ..
est ce que tu fais des plus gros chiffres quand tu lances loin ?.
hé Antoine si c'est pour faire des bêtises euh on joue pas aux petits dés et on joue à un autre jeu de mémo hein ..
non mais c'est parce qu'on t'attend maman ..
y a pas y a pas de problème ..
ben je prépare ton verre de grenadine ..
ouais attends xx ..
alors tiens ..
on va commencer ..
alors vas y ..
commence ..
un on peut pas on peut rien faire ?.
il faut un six ..
han ..
et si on fait un six on rejoue ?.
un ..
oh dis donc ..
cinq ..
pareil ..
j'ai fait comme toi ..
oh tu crois que je vais faire comme toi encore ?.
non ?.
tu crois pas ?.
oh ..
si peut être ..
ah ben non ..
ah attention ..
xxx ..
le verre ..
hé tu fais attention parce que tu es un petit peu gauche toi quand même un petit peu maladroit ..
euh je vais voir si on peut pas ..
bravo ..
mettre un ....
xx ..
tu aimes bien la grenadine ..
ah regarde Antoine ..
ah regarde ce qu'elle fait maman ..
xxx ..
on va mettre de l'autre côté ..
bah je vais te le donner ..
tu sais tu sais d'où ça vient ça ?.
regarde xxx ..
d'Australie ..
voilà ..
tu as pas fait le six encore ?.
et toi non plus ..
j'ai joué pour toi mais xxx j'ai pas réussi à te faire un six ..
allez à toi ..
ah !.
ah là je vois que il va y avoir des je te sens bien toi ..
hé on triche pas Antoine hein ..
juste la chance ..
ben non faut que tu //..
voilà ..
xxx ..
hum hum ben à toi mon amour ..
qu'est ce qu'on t'a appris aussi quand tu es là tu peux aller directement ..
tu les connais maintenant hein ?.
à toi mon coeur ..
han ..
encore un six ..
hé mais tu préfères avancer celui ci tu veux pas sortir l'autre ?.
non mais c'est comme tu veux mon amour moi je ....
c'est toi qui décides hein ..
voilà c'est très bien tu vois cinq tu as fait ..
bon ..
xx ..
ben dis donc tu pourrais quand même donner des six à maman ..
là c'est quoi mon coeur ?.
si ?.
non si j'étais ....
non si ....
voilà ..
super bravo ..
oui ..
et là tu as fait un cinq tu avances lequel ?.
mais alors là ..
fais attention si tu avances celui là ..
regarde là où ça te met ..
non ..
ben ..
rejoue pas avance ton cinq faut que tu le joues ..
non non tu as fait un cinq Antoine ..
tu avances soit celui ci soit celui ci mais tu ..
ouh la attention si si maman fait un six elle te mange ..
voilà alors attention si je fais un six ..
paf@i ..
chbing@i il fallait réfléchir ..
parce que j'xxx voilà ..
c'est vrai que hé non non ..
hé c'est elle rejoue maman ..
un ..
hé hé tu as fait deux ..
tu vois que tu commences à tricher là ..
tu avances mon coeur tu as de la chance tu en as encore tu en as un ..
hé hé ..
non non deux ..
là ..
ouh la la il triche hein ..
tu étais où ?.
d'accord xx ..
bon ben alors ..
voilà stop@s it@s ..
bravo ..
six tiens tu vois maman maintenant elle avance ..
oh six ..
un deux trois quatre cinq six ..
oh ben là j'ai eu de la chance ..
cinq ..
un deux trois quatre cinq ..
avance vite parce que sinon je vais te manger ..
un ..
un deux trois quatre cinq six ..
hé il faut que tu rejoues hein ..
Tania ..
ah oui mince oui oui c'est vrai ..
attends attends Antoine ..
attends attends non tu ouais ..
j'ai oublié ouais ..
un ..
oh la vite faut que tu sortes pour la manger ..
cinq un deux trois quatre cinq ..
avance vite hein mon amour parce que sinon ..
ben hé Antoine qu'est ce que je t'ai dit ?.
quand tu as cinq tu peux te mettre ..
voilà ..
han ..
deux ..
oh ben dis donc ..
oh faut pas faire un deux hein maintenant ..
hum un ..
xxx qu'est ce que tu //?.
ben comme tu hou hou hou j'ai peur ..
ouh la la la la ..
coquin ..
trois ..
un deux trois ..
pour te faire quoi mon coeur ?.
ah cinq ..
mais attention elle va te manger ..
oui ..
tu as intérêt d'avancer celui ci pour être sauvé hein ..
hum ..
ben ..
cinq ..
un deux trois quatre cinq ..
ouh si maman elle fait six ou quatre tu es mort ..
hum ..
non quatre ça fait rien ..
xxx ..
c'est incroyable ..
oh le pauvre vieux ..
chpling@i ..
six ben tu vois j'ai encore fait six tu vois de toutes façons ..
deux ..
bon ben je vais attendre hop ..
moi il faut que je fasse un trois ..
vas y ..
ben hé !.
ben tu tu sais tu vas où là directement quand tu as cinq là ..
voilà ..
cinq ..
un deux trois quatre cinq ..
mais non moi c'est ici que je monte mon amour ..
donc là il faut que je fasse combien ?.
ben voilà ..
ben je vais attendre de faire un trois ..
ben cinq avance toi ..
six ..
ah c'est cassé xx ..
un ouais ..
xx ..
xxx il est en train de sortir ..
attends elle arrive ..
elle arrive ..
han ..
Antoine mets toi bien j'ai j'ai toujours roh@i ..
voilà ..
hé hé c'est à moi ..
trois ..
ah ben trois génial faut que je fasse un un maintenant vite ..
bon d'accord ok ..
oui ben vas y ..
non cinq là ..
attention je surveille ..
six ..
ben parce que je te connais ..
cinq un deux trois quatre cinq ..
c'est bien mon coeur ..
ah ben oui tu es bloqué voilà ..
à moi ..
hé c'est à moi ..
attention hein ..
cinq un deux trois quatre cinq ..
ouh la la la ..
si tu fais un un je suis mort ..
morte ..
morte pardon ..
six ..
un deux trois quatre cinq six ..
xxx si tu fais encore six et six ..
hum ..
quatre à toi mon coeur ..
non tu es bloqué ..
faut que tu fasses un un pour me manger ..
ah ben un ..
ouf sauvé ..
à toi mon coeur ..
xxx ..
hé tu as coupé le ?.
euh non j'ai pas xxx ..
et tu rejoues ..
oh là ..
comment c'est pas possible ..
si si et puis il a pas triché ça va ..
ah bon ?.
allez rejoue ..
voilà c'est fini ..
là tu as pas mis juste au dessus ..
voilà ..
trois six de suite dis donc ..
trois six de suite ..
ça fait combien ça trois six ?.
ça fait beaucoup ..
deux six ça fait combien ?.
six et six ?.
oui douze ..
c'est bien mon coeur ..
bravo ..
bravo bravo ..
tu lui as dit ?.
non il a trouvé tout seul ..
attention ..
à moins qu'il ait lu dans ma tête mais //..
alors c'est à moi ?.
c'est ça ?.
ouais ..
euh xx ..
un deux trois quatre cinq ..
c'est très bien mon amour ..
non ?.
ah mais je te crois ..
et tu as pas voulu sortir ton autre petit cheval ?.
ben écoute c'est ton choix ..
hum quatre ..
d'accord ..
je vois qu'il est pour la la paix des ménages lui ..
xxx ..
vas y mon amour ..
ok ..
ah ben je suis bloquée moi ..
ah ..
ah ben tu rejoues pas ..
oui pardon oui c'est moi j'étais dans les vapes ..
cinq ..
xxx ..
ah tu vois on est bloqué là ..
ouais ..
qu'est ce qu'il faut que tu //?.
ah ..
xxx ..
ah ben moi aussi ..
coucou ..
ils sont tous les deux ..
à toi ..
ben trois après le un c'est quoi mon amour ?.
vas y ..
xxx ..
ben tu rêves ..
hé hé la prochaine fois je te dis rien hein tant pis pour toi ..
six je rejoue ..
quatre ..
qu'est ce qu'il faut que tu fasses comme chiffre ?.
ah bon ..
allez ..
demande ..
demande un petit six ..
oh ..
xx ..
xxx ..
xxx ..
ben dis donc hein ..
il se passe pas grand chose dans ce dans ce jeu là ..
xxx ..
on est coincé là hein ..
quatre ..
oh la la c'est pas ..
ah deux ..
d'accord ok j'ai vu ..
bon ben alors à moi ..
trois ..
hop ..
à toi ..
ah oui mais c'est pas six ..
hum ..
xxx à toi ..
ben tu es déjà xx ..
à toi ..
han !.
c'est quoi ?.
hé tu sors pas un cheval ?.
ben vas y alors ..
ben hé tu rêves ou quoi ?.
xxx tu avances pas vite hein ..
six attends c'est à moi de jouer ..
xx ..
deux ..
han ..
ah bah tu vas avancer un petit peu plus vite quand même ..
deux ..
n'oublie pas non plus Antoine si tu as un trois vaut mieux monter le petit cheval là hein ..
hein tu le sais quand même ..
vas y ..
hum ..
hum hum ..
c'est à moi hein ..
hé tu peux jouer ah non ..
non ..
hum ..
tu crois que tu vas réussir à rattraper maman ..
xxx tu penses que tu vas réussir ?.
la dernière fois qui c'est qui a gagné ?.
c'est bien là tu as bien //..
la dernière fois qui c'est qui a gagné ?.
c'était papa qui avait gagné ?.
ah bon c'était pas toi qui avais gagné quand on avait joué avec mamy ?.
c'est à moi de jouer là ..
deux ..
hé hier tu as joué avec mamy ?.
est ce que tu as joué avec mamy hier ?.
bah ..
non ?.
tu as fait quoi avec mamy hier ?.
tu sais plus ?.
ben tu as une petite mémoire hein ..
allez ..
joue ..
allez tu avais fait un deux ..
ah mince ..
faut le jouer ..
tu as de la chance que j'ai pas vu ..
mais que j'ai pas //..
qu'est ce tu fais avec ton quatre ?.
allez avance ..
qu'est ce que tu fais ?.
tu veux faire quoi ?.
tu //..
tu en es sûr ?.
bon si c'est ton choix ..
à maman de jouer ..
six ..
je recommence ..
hé ..
trois ..
hé te es un petit coquin tu voulais me le cacher ..
de quoi ?.
ben ça va là tu es tranquille pour avancer ..
ah oui j'avais pas vu c'est très bien mon amour ..
cinq ben non je peux pas ..
est ce que vous vous êtes reposés à l'école là ?.
non ?.
qu'est ce que tu as fait à l'école là après ..
tu veux que je te l'avance ?.
oui ?.
ah oui xxx oui ..
qu'est ce que tu as mangé mon chéri ?.
oui de la poire et puis ?.
tu as pas mangé du fromage ?.
et des chips ?.
xxx ..
tu as mangé des chips ?.
oui parce qu'en fait ils avaient mangé avant de partir à xx ..
tu as mangé dehors les chips alors ?.
non c'était dans la /..
à l'école ?.
oui c'était dans la salle de classe oui ..
vous aviez mangé à onze heures moins le quart dix heures non à dix heures même parce que je suis arrivée à dix heures et quart ..
à dix heures tu as mangé ton sandwich au pâté ..
hé Antoine tu joues ..
et après là xx ..
ah oui excuse moi oui je je parle et puis heu vas y ..
hé euh par contre euh ..
oui je peux t'avancer un deux trois quatre ..
oh tu es un peu fainéant ..
vas y ..
euh non à moi ..
je savais pas que tu aimais autant le pain d'épices ..
tu aimais bien le pain d'épices que Emmanuel a pris là t'a t'a donné ..
ben écoute je t'en achèterai ..
hein ?.
tu m'écoutes ?.
tu en voudras ou pas ?.
cinq ..
ben oui si tu en veux pas tu en veux pas de toutes manières ..
xx ..
non y a pas de problème non plus que je t'en achète mon amour ..
hein ?.
han ..
tu as /..
quatre ah ben je suis bête ..
bon c'est un petit peu cassé mais je t'autorise quand même ..
ben parce qu'il était en mais bon c'est bon xx ..
à cheval ..
à cheval ..
ah quand il est comme ça tu vois enfin tu vois quand il tombe euh ..
à cheval ..
à cheval comme ça ..
à la fois sur le tapis et par terre ..
hein ?.
non mais je te l'ai autorisé ..
euh ....
faut que tu rejoues là ..
faut que tu rejoues mon amour ..
trois ..
hé euh Antoine qui c'est qui a déménagé ?.
comme petit garçon ou petite fille ?.
ah d'accord ..
d'accord ..
ok ..
ok ..
xx ..
et puis toi tu es un quoi ?.
tu es un grand ?.
non un moyen ..
tu es un moyen aussi ..
ben oui Christophe ..
la hé Antoine Parisse !.
grand ce sera l'année prochaine ..
et aujourd'hui qui c'est qui était absente ?.
xxx d'accord ..
c'est comment ?.
ah xx ..
d'accord ..
hé hé c'est à moi ?.
et le petit garçon là il s'appelle comment ?.
c'est Kilian Kilian ?.
ah Kélian ..
d'accord ..
à toi de jouer ..
trois mais je peux pas jouer ..
hum quatre ..
je peux avancer ..
hum hum ..
hum là j'approche de la victoire ..
à toi ..
oh comme ça c'est génial comme ça tu as plus qu'à faire ..
six ..
je rejoue attention ..
six je rejoue ..
cinq ..
ha ha ha à toi mon amour ..
ben tu rejoues ..
oui ..
six ..
ah ..
et là il faut que je fasse deux fois un ..
oh dis donc c'est compliqué votre règle ..
xx ..
ouais ça c'est mamy ..
il faut faire un un pour sortir ..
je pensais que là arrivé là on avait gagné mais non ..
c'est peut être pas toi tout le temps qui gagne ..
ben là c'est à maman de jouer ..
non mais je pensais que quand on était arrivé à là ça y est c'était fini ..
on avait gagné ..
mais mamy hé ben enfin si tu lis la règle il faut faire un un pour sortir et pour gagner ..
mais tu as pas joué ..
ben joue mon coeur maintenant ..
six ..
à toi de nouveau ..
un ..
enfin c'est moi qui dois faire des //..
un ..
ça y est ..
j'ai gagné ..
enfin un cheval ..
deux ..
tu avances là ..
à mon avis ça va être maman ..
oh mince deux ..
deux fois trop ..
hum ..
xxx tu avances vite toi par contre ..
ah bon ..
ah ..
ah ben hé hé ..
j'ai gagné ..
elle a gagné ..
ah ..
non ben écoute on peut on peut jouer au mémo ..
on va jouer à autre chose ..
au mémo de cars hé ..
au mémo d'accord ok ..
ah ben vas y apprends moi ..
alors par contre on met //..
il est où le mémo ?.
ouais tout à fait ouais ..
c'est moi qui ....
alors attends on on range déjà ..
tu gagnes souvent au mémo ?.
xx ..
tu gagnes souvent au mémo ?.
chez mamy tu avais souvent gagné hein quand on a joué ..
alors ..
non non on les met là mon chéri ..
oui ..
non mais c'est bon on n'en a pas besoin de ça c'est bon ..
non non ..
ça va nous gêner ..
xx ..
ouais ça va nous gêner ..
alors tu m'expliques comment on joue ..
hein Antoine ?.
ah oui ..
tu m'expliques comment on joue ?.
ah ben non ..
alors attention ..
tu retournes toutes les cartes ?.
non hé hé Antoine tu m'attends ..
attention hein ..
oui ben hé cinq minutes si tu m'aidais aussi ça ....
allez ..
mets en en place ..
hop ..
attends oui ..
attends je vais //..
oh y en a deux de trop ..
oui enfin après c'est pas forcément ..
alors comment on fait ?.
explique moi Antoine ..
moi j'ai pas compris tu retournes des cartes mais ..
tu attends excuse moi mon chéri ..
attends je vais te les approcher un petit peu plus de //..
de toi parce que tu as pas des maman elle a les plus longs bras ..
xxx ..
maman elle a le bras long ..
oui ..
et tu es forte aussi ..
xx ..
hein ?.
certainement certainement //..
alors explique à tonton ..
xx ..
alors tu as retourné puis maintenant qu'est ce que tu fais ?.
donc elles sont pas identiques ..
donc il faut chercher deux identiques ..
et si elles sont identiques ..
qu'est ce qu'il se passe ?.
qu'est ce qu'il se passe quand elles sont xx //..
on les on les met là ..
et on rejoue ..
voilà ..
hum hum alors ..
c'est maman maintenant ..
ce qui l'intéresse c'est de rejouer ..
voilà xx ..
ce que tu aimes bien c'est rejouer ..
tu as bien vu hein ?.
ben ne fais pas à chaque fois les mêmes Antoine ..
ben écoute sinon on n'avancera pas ..
bon ..
alors ..
ben c'est pas évident parce qu'elles se ressemblent beaucoup hein ..
oui ..
c'est pas évident xx mais lui ..
ah ..
xx ..
pas mal bel essai ..
hou hou ..
xxx ..
ben elle retourne la deuxième ..
hé ..
hé hop voilà ..
un pour maman ..
alors attention ..
c'est c'était de la chance là parce que xx ..
non à toi mon chéri ..
bien joué ..
ah là maman elle est scotchée ..
euh ..
non non c'était pas celle là attends ..
ah mince ..
ah tu as vu on avait déjà fait celle ci ..
hop Ramone ..
on n'a pas pris Ramone ..
ah y a Ramone et y a Sally ..
pas celle ci ..
à toi mon amour ..
attention tu on va les remettre bien parce que sinon après on n'y arrivera pas Antoine hein ..
Sally et Luigi c'est pas là ?.
ah non ..
mince ..
ah je savais bien que c'était ..
hé oui ..
attends ..
ils sont par là non ?.
attends celle ci on l'avait pas vue ?.
roh@i ..
bon ..
Ramone et Luigi on les as pas //..
Ramone et Luigi ..
oh elle est bonne maman ..
à moi ..
ah ..
Flash Mc Queen et Luigi on l'a vu non ?.
c'est par là non ?.
ouh la la ..
ben oui hein tu sais c'est important la mémoire ..
alors Luigi et Martin ..
Antoine j'arrête ..
assis mets toi assis normalement ..
sinon tu vas tomber ..
à toi mon amour ..
ben oui ..
xxx alors Martin ..
hum ..
hum ..
ouh@i dis donc il te rattrape là ..
euh Sally et Ramone ..
non ..
attention attends tu les remets bien mon chéri hein ..
roh@i Ramone et attends ..
on va chercher han mince !.
c'est Sally et Ramone hein attention mon chéri ..
oh ..
ben pourquoi je fais une tête comme ça parce que là tu en as beaucoup là ..
ah ..
ouais ..
Ramone et Martin ..
ah non c'était Ramone et Martin ..
et Ramone ..
oui ..
ça va là elles sont pas trop loin ?.
Ramone et Martin ..
non ah par contre ..
non c'est ni Martin Luigi ..
xxx ..
hum ..
hum hum ..
Antoine ..
Ramone attends Ramone et Sally ..
oh la la ..
Ramone euh pas Ramone et Sally Ramone et Flash Mc Queen pardon ..
ouh la ..
alors là faut pas que tu te trompes ..
faut pas que je me trompe ..
parce que c'est la ....
ah c'est là ..
défaite ..
ah mince ..
bad@s move@s ..
alors on les compte ..
alors un deux trois quatre cinq six ..
et toi ?.
donc qui c'est qui a gagné ?.
hum ..
d'accord ..
tu bois un petit peu ?.
tu reveux un petit café ?.
parce que /..
je veux bien ..
si il en reste ..
oui ..
il en reste ..
hé ben dis donc ..
qu'est ce que je fais ?.
on refait la revanche ?.
tu je vais essayer de gagner moi ?.
de quoi ou ?.
ben on tu veux refaire une autre partie de mémo ou pas ?.
d'accord ah par contre c'est moi qui les mets hein ..
faut bien les mélanger hein parce que toi je te /..
oui je les mettrai oui attends je vais servir un café à tonton ..
ça //..
te mets pas comme ça tu vas tomber ..
mets toi assis ..
tu crois que tu vas gagner encore une fois ?.
c'est xxx mais pourquoi c'est toi qui gagnes ?.
ben ..
tu aimes bien gagner ?.
c'est étonnant ..
c'est étonnant ..
c'est un peu fort quand même le café je trouve ..
il est très bon ..
hum ..
oui hum ..
il est très bon ..
hein Antoine il est bon ?.
alors c'est quoi ton jeu préféré ?.
c'est celui là ?.
attends je vais juste ranger le dé mon amour ..
parce que sinon après si y a Guillaume qui le voit le dé ..
attention ..
qu'est ce qu'il fait si Guillaume il voit le dé ..
il va le perdre ?.
ou il va le manger ?.
est ce qu'il sait jouer aux petits chevaux ?.
Guillaume ..
et est ce qu'il sait jouer au mémory ?.
il sait jouer au mémo ?.
quoi ?.
attends déjà je mélange bien tout ..
tiens ..
t'inquiète pas ..
hé hé ..
mais tu as pas besoin de tricher de toutes façons tu joues mieux que maman ..
qui est ce qui gagne le plus souvent ?.
maman ou toi ?.
attends mon coeur ..
de quoi mon chéri ?.
mais mais laisse les comme ça ..
hé ..
je je les laisse ..
ah tu veux dire que tu as pas besoin de les ranger ..
ah bon ?.
non non c'est quand même plus facile euh ..
quoi ?.
qu'est ce qu'y a mon amour j'ai pas compris ?.
tu es capable ?.
c'est ça ?.
un gamin ?.
tu es un gamin toi ?.
tu es un petit gamin ?.
tu es mon petit gamin ?.
tu sais ce que c'est qu'un gamin ?.
c'est un petit garçon ..
un petit gamin c'est pour dire un petit garçon mon chéri ..
et gamine une petite fille ..
ah ben dis donc tu es prêt toi ..
hé ..
ben écoute j'étais pas prête hein ..
ben hé ne retourne pas chaque fois les mêmes ..
tu fais comme papy Armando toi ..
oui mais en l'occurrence c'est lui qui gagne ..
oui ..
ne pense pas qu'elles sont l'une à côté de l'autre parce que j'ai fait euh ..
je les ai mélangées mon amour alors là euh ..
attends voir ..
Ramone et Luigi on les as pas déjà vus ?.
attends ..
hum ..
on a du mal là hein ..
hé hé ..
quoi ?.
ben voilà et tu fais une autre ..
ah bon ?.
là maman est dégoûtée ..
et en plus ça c'est filmé ça ..
ben ça prouve qu'il y arrive ..
hum ..
c'est le bon fond mais c'est pas la bonne forme ..
mince ..
comment ça non ?.
ha ha ha ..
ha ha ha ..
nanana@i ..
nana@i ..
ah ..
tu as retenu ?.
hein fais pas ton petit //..
il a très bien retenu à mon avis ..
ah ..
raté ..
tu as loupé ..
ah mince xxx ..
excuse moi mon chéri ..
c'est pas bon ..
te@i ah zut euh mince ..
euh mince mince ..
mince xx ..
hein xx ..
oui c'est bon ..
tu as de la chance là hein ..
non ?.
xxx ..
ah Martin il est là ..
ah non mince ..
euh ..
bon on va essayer de faire un petit peu là bas parce que là bas on n'a pas trop fait encore hein je trouve ..
d'accord ..
Sally ..
non ..
le cerveau de maman il fume là ..
ah non ça je sais plus ..
je fatigue moi ..
xxx ..
toujours des voitures des voitures ..
hum hum ..
hum ..
quoi c'est là ?.
sûr ?.
oh ..
ah la la il te fait croire n'importe quoi hein ..
hé ..
xxx tu es un petit filou là ..
ben ..
quatre ..
hum hum hum ..
ah mince ..
oh !.
bête ..
j'ai été trop vite ..
ah là dis donc ..
je suis nulle hein ..
je fatigue . ..
ouh@i ben alors là franchement ..
tu en as combien ?.
han encore plus que la dernière fois ..
xxx ..
quatre ..
oh ben dis donc ..
je suis nulle là hein ..
mais non je suis ..
je me suis pas bien concentrée ..
de quoi ?.
tu as fermé les yeux pourquoi ?.
quand ?.
ah je croyais que tu allais dire j'ai quand même triché ..
tu as gagné sans tricher ..
non mais tu as pas triché ..
et la fois d'avant tu avais triché ?.
mais non ..
et est ce que tu joues avec ce jeu là avec mamy mamy Claude ?.
non ?.
tu as jamais joué avec mamy Claude à ça ?.
mamy Claude avec toutes les voitures elle va euh //..
tu crois que //..
il faudrait que tu joues avec mamy Claude aussi à ce jeu là ..
qu'est ce tu en penses ?.
à quel jeu tu joues avec mamy Claude ..
on fait la dernière partie alors ..
ok ..
ben parce que après euh ....
ben ..
elle elle voudrait gagner une fois ..
xx ..
mais tu as gagné aux petits chevaux ..
ah ben oui j'ai gagné aux petits chevaux ..
on peut pas tout on peut pas gagner partout ..
tiens bois encore un petit peu ..
tu bois un petit peu mon amour ..
ouais non ..
regarde moi ..
tu peux boire un petit verre mon chéri ..
ah c'est quoi que tu bois ?.
du vin ?.
et de la grenadine c'est quoi ?.
c'est pas du vin ?.
c'est jamais du vin ?.
si si ..
ouais ..
ah pourquoi parce que tu aimes pas ?.
et pourquoi ?.
han ..
point final ?.
hé hé Antoine je ne veux pas que tu parles comme ça à tonton hein ..
oh ..
attention hein ..
ben non y a pas de point final ..
c'est à la rigueur ce sont les adultes qui disent point final ..
point final mais pour combien de temps ..
tu boiras jamais de vin dans ta vie ?.
xxx ..
quand tu seras grand tu crois que tu boiras pas de vin ?.
hé écoute tonton ..
quand tu seras grand est ce que tu penses boire un peu de vin et de champagne ?.
pourquoi ?.
han !.
mais quoi ?.
mais non je triche pas moi ..
tu es un petit peu rouge là dessus xxx ..
parce que tu as bu du vin ..
non je sais pas ..
mais non j'ai pas bu de vin ..
tu énerves pas mon chéri ..
en plus xxx ..
au début c'est dur hein ..
ouais ..
il faut pas que tu retournes toujours les mêmes quand même ..
beh si on retourne les mêmes mon amour on y arrivera jamais !.
ha ha !.
celle là on l'a déjà vue non ?.
oui je m'en souviens plus moi aussi ..
ben hé je t'écoute plus ..
hum ..
quoi ?.
ah tu as vu xxx mon coeur ..
hé Antoine par contre tu te mets correctement assis sinon moi j'arrête de jouer ..
tu veux que j'allume ?.
non ça va ..
oh la ..
hé tu l'as ....
montre voir ..
d'accord ..
ok ..
euh ..
ha ha ..
hum xx ..
j'ai eu chaud ..
pourquoi ?.
euh Ramone ..
Ramone Ramone ..
ah ..
hum ah non c'est pas celle là non plus ..
ben ..
ben vas y ..
fais comme tu veux ..
mais là xx ..
non tu as pas triché jusque là tu as eu de la chance parce que à mon avis tu savais pas ..
hé je t'écoute plus hein !.
ben tu as qu'à le faire toi ..
il t'écoutes tu l'écoutes pas mais lui il sait où sont les cartes ..
hé !.
tu me la mets comme il faut hein ..
ah mais je sais pas moi ..
pique et pique et colégramme ..
amstram ....
gramme ..
quatre doigts dans la bouche dis donc ..
ben dis donc ..
ouh@i ah ..
ben hé réfléchis rapidement mon amour ..
de toutes façons c'est pas grave sinon euh ..
hé ..
boh@i ..
hum ..
quoi ?.
oh ..
ah quoi ?.
Luigi il était pas là ?.
les bruits qu'il fait ..
oh ..
bravo ah ..
Luigi et Ramone ..
hé doucement ..
hé Antoine ..
xxx je sais plus moi ..
xxx ..
vous avez du mal là ..
ouais on a du mal ben tu vois on fatigue ..
Luigi ..
bien mon chéri ..
heureusement que tu es là ..
oh ben dis donc ..
il a du mal enfin vous avez du mal euh pas tout le monde ..
ouais ouais xxx non ..
ah ..
Sally Sally ..
ah mince ..
ah ben ..
attention Antoine tu risques de tomber et de te faire ....
très mal ..
moi je vais filmer ça quand il tombe ..
hum hum badaboum ..
allez let@s s@s go@s ..
joking@s ..
hum un coup de chance là ..
comment comment ?.
oh la ..
là maman elle ramasse tout le reste ..
ouh la ouh la ..
attention ça se déchaîne là ..
ah mince c'est pas celui là ..
hum ..
j'aime bien le non non ..
