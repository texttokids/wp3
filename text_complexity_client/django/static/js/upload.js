$(document).ready(function (e) {
    let nbID = 0; //Un compteur pour la génération d'ID
    let Taille_max = 550000 //Taille maximum des fichiers en bit
    $('#upload').on('click', function () {
        var type_text = $("input[name='text']:checked").val(); //Stockage du choix text ou fichier
        var eM = document.getElementById('id_emailReg').value; //Stockage du mail
        var institution = document.getElementById('id_institution').value; //Stockage de l'institution
        
        //Protection contre mail vide
        if(eM == ""){
            $('#msg').html('<div class="alert alert-danger" role="alert">Entrez votre email</div>');
            return;
        }
        //Protection contre institution vide
        if(institution == ""){
            $('#msg').html('<div class="alert alert-danger" role="alert">Entrez votre institution</div>');
            return;
        }
        //Upload de fichier
        if(type_text=="uploadfield"){

            var ins = document.getElementById('multiFiles').files.length;
            //Protection contre auncun fichier choisi
            if(ins == 0) {
                $('#msg').html('<div class="alert alert-danger" role="alert">Aucun fichier sélectionné</div>');
                return;
            }
            //Ajout des fichiers au formulaire
            for(var x = 0; x < ins; x++){
                if(document.getElementById('multiFiles').files[x].name.split('.').pop() == 'zip') {
                    JSZip.loadAsync(document.getElementById('multiFiles').files[x]).then(function(zip) {
                        for(let [zip_filename, zip_file] of Object.entries(zip.files)) {
                            if (zip_file._data.uncompressedSize > Taille_max){
                                $('#msg').html('<div class="alert alert-danger" role="alert">Au moins un fichier dans le zip dépasse la limite de 500 Ko</div>');
                                return;
                            }
                        }
                        upload();
                    }).catch(function(err) {
                        console.error("Failed to open", zip_filename, " as ZIP file:", err);
                    });
                }
                else if(document.getElementById('multiFiles').files[x].size > Taille_max){
                    $('#msg').html('<div class="alert alert-danger" role="alert">Au moins un fichier dépasse la limite de 500 Ko</div>');
                    return;
                }
                else {
                    upload();
                }
            }
        }
        //Traitement par zone de texte
        else if(type_text=="textarea"){
            upload();
        }
        //Protection contre aucun choix de traitement
        else{
            $('#msg').html('<div class="alert alert-danger" role="alert">Aucun type de texte sélectionné</div>');
            return;
        }
    });

    function upload(e) {
        var form_data = new FormData(); //Création d'un objet Formulaire
        var type_text = $("input[name='text']:checked").val(); //Stockage du choix text ou fichier
        var eM = document.getElementById('id_emailReg').value; //Stockage du mail
        var institution = document.getElementById('id_institution').value; //Stockage de l'institution
        
        $('#msg').html('');
        
        //Upload de fichier
        if(type_text=="uploadfield"){
            var ins = document.getElementById('multiFiles').files.length;
            //Ajout des fichiers au formulaire
            for (var x = 0; x < ins; x++) {
                form_data.append("files", document.getElementById('multiFiles').files[x]);
            }
            //Reset de la sélection de fichier
            $('#multiFiles').val(null);
        }
        //Traitement par zone de texte
        else if(type_text=="textarea"){
            var text_val = $("#zonetext").val();
            //Protection contre zone de texte vide
            if(text_val == ""){
                $('#msg').html('<div class="alert alert-danger" role="alert">Zone de texte vide</div>');
                return;
            }
            form_data.append("textarea_val", text_val);
        }

        //Ajout de toutes les sélections au formulaire
        form_data.append("text", type_text); 
        form_data.append("emailReg", eM);  
        form_data.append("institution", institution);
        //form_data.append("traitement", $("input[name='traitement']:checked").val());
        
        csrf_token = $('input[name="csrfmiddlewaretoken"]').val();                     
        form_data.append("csrfmiddlewaretoken", csrf_token);                     

        var levels = $('input[name="levels"]:checked').map(function (){return $(this).val();}).toArray();
        levels.forEach(element => {
            form_data.append("levels", element);
        });

        var ls_processors = ["processors", "graphie", "phonetique", "lexique", "morphosyntaxe", "syntaxe", "semantique", "formules_niveau"];
        ls_processors.forEach(processor => {
            $('input[name="'+ processor +'"]:checked').each(function (){
                form_data.append("processors", $(this).val());
            });
        });

        //Lancement de la requête en AJAX   
        let ajax_rq_id = $.ajax({
            url: '', //Lien d'envoi des données
            dataType: 'json', //Réponse serveur
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, //Formulaire à transmettre
            type: 'post',
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                var lsFilesName = form_data.getAll("files");
                var filesName = "";
                //Pour l'affichage des noms de fichier lors de l'upload
                for (let i = 0; i < lsFilesName.length; i++) {
                    if(filesName.length > 100){ 
                        filesName += " ... ";
                        break;
                    }
                    filesName += lsFilesName[i]['name'];
                    if(i != lsFilesName.length-1){
                        filesName += ", ";
                    }
                }
                // Upload progress
                nbID += 1;
                var nbID_local = nbID;
                var upload_div = //Création du div pour la barre de chargement
                "<div class='row' id='filesName" + nbID_local+"'>" +
                    "<div>" + filesName + 
                        "<span class='float-end'><span style='padding-right: 0.5em;' id='load" + nbID_local + "'></span><button class='btn btn-danger' id='btn" + nbID_local + "'>X</button></span>" +
                    "</div>"+
                    "<div class='progress g-0'>"+
                        "<div class='progress-bar' id='progress_bar" + nbID_local + "' role='progressbar' style='width: 0%;' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'>0%</div>" +
                    "</div>" +
                "</div>";

                $("#progress_div").append(upload_div);
                $('#btn'+nbID_local).on('click', function(e, id=nbID_local, ajax_rq=ajax_rq_id) { //Bouton d'annulation
                    ajax_rq.abort();
                    $('#filesName'+id).remove();
                });
                //Progression de la barre
                xhr.upload.addEventListener("progress", function(evt,id=nbID_local){
                    if (evt.lengthComputable) {
                        var percentage = (evt.loaded/evt.total*100|0);

                        document.getElementById('progress_bar'+id).style["width"]="" + percentage + "%";
                        document.getElementById('progress_bar'+id).innerHTML="" + percentage + "%";

                        $('#load'+id).text(evt.loaded + "/" + evt.total);

                        if (percentage == 100){
                            setTimeout(() => { 
                                $('#filesName'+id).remove();
                                //document.getElementById('filesName'+x).style["color"]="#08FF08";
                            }, 2000);
                        }
                    }
                }, false);
                return xhr;
            },
            success: function (response) { //Réussi
                if($.active == 1) {
                    $('#msg').html(response.msg);
                }
                else {
                    $('#msg').html("<div class='alert alert-info' role='alert'>Files are still uploading ...</div>");
                }
            },
            error: function (response) {
                $('#msg').html(response.message); //Erreur
            }
        });
    };
    //Message en cas de tentative de fermeture de la page avant la fin des uploads
    window.onbeforeunload = function() {
        if ($.active > 0) {
            console.log("Upload not finish");
            return 'Upload not finish';
        }
    }
});