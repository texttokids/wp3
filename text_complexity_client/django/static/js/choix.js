$(document).ready(function (e) {
    choix();
    $("input[type='radio']").on('click', function(){choix();});
    function choix() {
        var type_text = $("input[name='text']:checked").val();
        if(type_text=="textarea"){
            $('#text_type').html("<textarea name='text' cols='40' rows='10' required='' id='zonetext'></textarea>");
        }
        else if(type_text=="uploadfield"){
            $('#text_type').html("<label>Vos fichiers : <input type='file' accept='.txt,.zip' id='multiFiles' name='files[]' multiple='multiple' style='padding-left: 0.5em;'/></label><br><em>Les fichiers doivent à traiter doivent être au format txt (ou txt dans un zip) avec un encodage utf-8<br>Les fichiers ne doivent pas dépasser 500 Ko (Un zip peut dépasser cette limite mais chaque fichier qu'il contient doit la respecter)</em>");
        }
    };

    $("#help_button").on('click', function(){
        $("#help").toggle();
    });

    $('#id_emailReg').on('keyup', function () {
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/;
        if ($(this).val().match(validRegex)) {
            $('#upload').prop('disabled',false);
            $(this).css("color", "green");
        } 
        else {
            $('#upload').prop('disabled',true);
            $(this).css("color", "red");
        }
    });

    $('input[type=checkbox]').on('click', function() {
        document.getElementById('help').scrollTo(0, 0);
        var form_top = $(this).offset().top;
        var value = $(this).val();
        if (value in ['text','sentence']){
            value = 'levels';
        }
        var help_top = $('#info_' + value).offset().top;
        document.getElementById('help').scrollTo(0, help_top - form_top + 50);
    });

    var ls_processors = ["processors", "graphie", "phonetique", "lexique", "morphosyntaxe", "syntaxe", "semantique", "formules_niveau"];
    ls_processors.forEach(element => {
        $('#select_' + element).on('click', function () {
            var state = false;
            $('#id_' + element + ' input[type=checkbox]').each(function () {
                if(this.checked == true){
                    state = true;
                }
            });
            $('#id_' + element + ' input[type=checkbox]').prop('checked', ! state);
        });
    });

});