from django.db import models
from django.core.files.storage import FileSystemStorage
import configparser

config = configparser.ConfigParser()
config.read('../../file_executer/settings.config')

inputs = FileSystemStorage(location=config['file_management']['inputs'])
outputs = FileSystemStorage(location=config['file_management']['outputs'])

class Fichiers(models.Model):
    email = models.CharField(max_length=255)
    institution = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    levels = models.CharField(max_length=255)
    processors = models.CharField(max_length=255)
    fichier = models.FileField(storage=inputs)
    statut = models.BooleanField(default=False)
    traitement = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name

class Resultats(models.Model):
    email = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    levels = models.CharField(max_length=255)
    processors = models.CharField(max_length=255)
    fichier = models.FileField(storage=outputs)
    traitement = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    date_fin = models.DateTimeField(auto_now_add=True)
    statut = models.BooleanField(default=False)
    def __str__(self):
        return self.name

class LogError(models.Model):
    email = models.CharField(max_length=255)
    error = models.TextField()
    fichier = models.FileField(storage=inputs)
    date = models.DateTimeField(auto_now_add=True)