from django.views.generic import TemplateView

class HomePageView(TemplateView):
    template_name = 'home.html'

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie, csrf_protect, requires_csrf_token
from django.contrib.admin.views.decorators import staff_member_required
from django.utils import timezone

import requests
import json
import sys
import os
import random
import string
import datetime
import sqlite3

from .models import Fichiers, Resultats, LogError
from .forms import ChainForm, SemanticsForm, ResultsForm

from os.path import exists
from zipfile import ZipFile

import configparser

config = configparser.ConfigParser()
config.read('../../file_executer/settings.config')

inputs_txt_file = config['file_management']['inputs']
output_dir = config['file_management']['outputs']
val_days = int(config['validation']['results_max_days'])

@csrf_exempt
@ensure_csrf_cookie
def chain(request):
    if request.method == 'POST':
        # POST, generate file stockage with data from the request
        form = ChainForm(request.POST)
        if form.is_valid():
            choice_levels = ','.join(form.cleaned_data['levels'])
            choice_processors = ','.join(form.cleaned_data['processors'])
            choice_mail = form.cleaned_data['emailReg']
            #choice_chain = form.cleaned_data['traitement']
            type_txt = form.cleaned_data['text']
            choice_institution = form.cleaned_data['institution']
            request.session['mail'] = choice_mail
            request.session['institution'] = choice_institution
            request.session['processors'] = form.cleaned_data['processors']

            if type_txt == "textarea":
                choice_text = request.POST.get('textarea_val')
                choice_name = ''.join(random.choice(string.ascii_letters) for i in range(10)) + ".txt"
                while exists(inputs_txt_file + choice_name):
                    choice_name = ''.join(random.choice(string.ascii_letters) for i in range(10)) + ".txt"
                with open(inputs_txt_file + choice_name, 'w') as fh:
                    fh.write(choice_text)
                    fh.close()
                Fichiers.objects.create(email=choice_mail,institution=choice_institution,name=choice_name,levels=choice_levels,processors=choice_processors,fichier=choice_name, traitement="analyse globale")
                return JsonResponse({'msg':'<div class="alert alert-success" role="alert">Files successfully uploaded</div>'})

            elif type_txt == "uploadfield":
                files = request.FILES.getlist('files', None)
                for f in files:
                    Fichiers.objects.create(email=choice_mail,institution=choice_institution,name=f,levels=choice_levels,processors=choice_processors,fichier=f, traitement="analyse globale")
                return JsonResponse({'msg':'<div class="alert alert-success" role="alert">Files successfully uploaded</div>'})

        return JsonResponse({'msg':'<div class="alert alert-danger" role="alert">Error</div>'})

    presets = {'levels': 'sentence', 'text': 'uploadfield'}
    if 'mail' in request.session and 'institution' in request.session:
        presets['emailReg'] = request.session['mail']
        presets['institution'] = request.session['institution']
    if 'processors' in request.session:
        for proc in ["graphie", "phonetique", "lexique", "morphosyntaxe", "syntaxe", "semantique", "formules_niveau"]:
            presets[proc] = request.session['processors']

    form = ChainForm(initial=presets)
    return render(request,'chain.html',{'form':form})
            


@csrf_exempt
@ensure_csrf_cookie
def semantics(request):
    if request.method == 'POST':
        # POST, generate file stockage with data from the request
        form = SemanticsForm(request.POST)
        if form.is_valid():
            choice_levels = ','.join(form.cleaned_data['levels'])
            choice_processors = ','.join(form.cleaned_data['processors'])
            choice_mail = form.cleaned_data['emailReg']
            #choice_chain = form.cleaned_data['traitement']
            type_txt = form.cleaned_data['text']
            choice_institution = form.cleaned_data['institution']
            request.session['mail'] = choice_mail
            request.session['institution'] = choice_institution
            request.session['semantic_processors'] = form.cleaned_data['processors']

            if type_txt == "textarea":
                choice_text = request.POST.get('textarea_val')
                choice_name = ''.join(random.choice(string.ascii_letters) for i in range(10)) + ".txt"
                while exists(inputs_txt_file + choice_name):
                    choice_name = ''.join(random.choice(string.ascii_letters) for i in range(10)) + ".txt"
                with open(inputs_txt_file + choice_name, 'w') as fh:
                    fh.write(choice_text)
                    fh.close()
                Fichiers.objects.create(email=choice_mail,institution=choice_institution,name=choice_name,levels=choice_levels,processors=choice_processors,fichier=choice_name, traitement="analyse détaillée")
                return JsonResponse({'msg':'<div class="alert alert-success" role="alert">Files successfully uploaded</div>'})

            elif type_txt == "uploadfield":
                files = request.FILES.getlist('files', None)
                for f in files:
                    Fichiers.objects.create(email=choice_mail,institution=choice_institution,name=f,levels=choice_levels,processors=choice_processors,fichier=f, traitement="analyse détaillée")
                return JsonResponse({'msg':'<div class="alert alert-success" role="alert">Files successfully uploaded</div>'})
        return JsonResponse({'message':'<div class="alert alert-danger" role="alert">Error</div>'})

    presets = {'levels': 'sentence', 'text': 'uploadfield'}
    if 'mail' in request.session and 'institution' in request.session:
        presets['emailReg'] = request.session['mail']
        presets['institution'] = request.session['institution']
    if 'semantic_processors' in request.session:
        presets['processors'] = request.session['semantic_processors']

    form = SemanticsForm(initial=presets)
    return render(request,'semantics.html',{'form':form})



def results(request):
    table = False
    if request.method == 'POST':
        form = ResultsForm(request.POST)
        if form.is_valid():
            emailName = form.cleaned_data['emailCheck']
            date_validation = timezone.now() - datetime.timedelta(days=val_days)
            fichier_mail = Resultats.objects.filter(email=emailName, date_fin__gte=date_validation)
            if len(fichier_mail) > 0:
                table = True
            return render(request, 'results.html', {'form':form,'table':table,'fch':fichier_mail,'dl_all':emailName})
    form = ResultsForm()
    if 'mail' in request.session:
        form = ResultsForm(initial={'emailCheck': request.session['mail']})
    return render(request, 'results.html', {'form':form,'table':table})


def download(request, file_id):
    uploaded_file = Resultats.objects.get(id=file_id)
    response = HttpResponse(uploaded_file.fichier, content_type='application/')
    response['Content-Disposition'] = 'inline; filename=' + uploaded_file.name
    return response



def download_all(request, mail):
    f_name = mail.split("@")[0] + ".zip"
    f_path = output_dir + f_name
    date_validation = timezone.now() - datetime.timedelta(days=val_days)
    ls_files = Resultats.objects.filter(email=mail, date__gte=date_validation)
    with ZipFile(f_path, 'w') as archive:
        infos = ""
        for file in ls_files:
            infos += "***** " + file.name + " *****\nLevels : " + file.levels + "\nProcessors : " + file.processors + "\nType : " + file.traitement + "\n\n"
            archive.write(output_dir + file.name, arcname="resultats/"+file.name)
        with open(output_dir + "infos.txt", 'w') as fh_info:
            fh_info.write(infos)
        archive.write(output_dir + "infos.txt", arcname="infos.txt")
    with open(f_path, 'rb') as fh:
        response = HttpResponse(fh.read(), content_type='application/zip')
        response['Content-Disposition'] = 'inline; filename=' + f_name
        return response

@staff_member_required
def admin_check(request):
    table = False
    get_first = list_position()
    if len(get_first) > 0:
        en_cours = Fichiers.objects.filter(id=get_first[0][1])[0]
    else:
        en_cours = None
    if request.method == 'POST':
        form = ResultsForm(request.POST)
        if form.is_valid():
            emailName = form.cleaned_data['emailCheck']
            #date_validation = timezone.now() - datetime.timedelta(days=val_days)
            fichier_mail = Fichiers.objects.filter(email=emailName).order_by('-date')#, date__gte=date_validation
            if len(fichier_mail) > 0:
                table = True
                pos = list_position(emailName)
                c = 0
                error_log = LogError.objects.filter(email=emailName).values_list('fichier')
                for file_obj in fichier_mail:
                    if not file_obj.statut:
                        file_obj.position=pos[c][0]
                        c += 1
                    if (file_obj.fichier,) in error_log:
                        lst_log = LogError.objects.filter(email=emailName, fichier=file_obj.fichier).values_list('error')
                        file_obj.log = lst_log[len(lst_log)-1][0]
                        file_obj.statut = 'Error'
                        #Fichiers.objects.filter(pk=file_obj.id).update(statut=True)
                    else:
                        file_obj.position="Fini"
            return render(request, 'check.html', {'form':form,'table':table,'fch':fichier_mail,'en_cours':en_cours})
    form = ResultsForm()
    return render(request, 'check.html', {'form':form,'table':table,'en_cours':en_cours})

def list_position(mail=None):
    conn = sqlite3.connect("./db.sqlite3")
    cur = conn.cursor()
    if mail != None:
        qry="SELECT position FROM (SELECT ROW_NUMBER() OVER() AS position, email, date FROM texttokids_fichiers WHERE statut=False) WHERE email='" + mail + "' ORDER BY date DESC"
    else:
        qry="SELECT ROW_NUMBER() OVER() AS position, id FROM texttokids_fichiers WHERE statut=False"
    try:
        cur.execute(qry)
        r = cur.fetchall()
    except Exception as e:
        print (e)
        print ("Error: unable to fecth data")
        r = None
    conn.close()
    return r