# forms.py in app named 'contact'
from django import forms

class ChainForm(forms.Form):
    LEVELS = (
        ('text', 'Texte'),
        ('sentence', 'Phrases'),
    )

    PROCESSORS = (
        ("graphie", "Graphie"),
        ("phonetique", "Phonétique"),
        ("niveau_lexical", "Niveau lexical"),
        ("richesse_lexicale", "Richesse lexicale"),
        ("adjectifs_ordinaux", "Adjectifs ordinaux"),
        ("flexions_verbales", "Flexions verbales"),
        ("parties_du_discours", "Parties du discours"),
        ("pronoms", "Pronoms"),
        ("pluriels", "Pluriels"),
        ("dependances_syntaxiques", "Dépendances syntaxiques"),
        ("structures_passives", "Structures passives"),
        ("structures_syntaxiques", "Structures syntaxiques"),
        ("superlatifs_inferiorite", "Superlatifs inferiorite"),
        ("adverbiaux_temporels", "Adverbiaux temporels"),
        ("modalite", "Modalité"),
        ("emotions", "Emotions"),
        ("emotyc", "Emotyc"),
        ("connecteurs_organisateurs", "Connecteurs organisateurs"),
        ("entites_nommees", "Entités nommées"),
        ("propositions_subordonnees", "Propositions subordonnées"),
        ("metaphores", "Métaphores"),
        ("formules_lisibilite_standard","Formules lisibilité standard"),
        ("age", "Age"),
    )

    GRAPHIE = (
        ("graphie", "Graphie"),
    )

    PHONETIQUE = (
        ("phonetique", "Phonétique"),
    )

    LEXIQUE = (
        ("niveau_lexical", "Niveau lexical"),
        ("richesse_lexicale", "Richesse lexicale"),
        ("adjectifs_ordinaux", "Adjectifs ordinaux"),
    )

    MORPHOSYNTAXE = (
        ("flexions_verbales", "Flexions verbales"),
        ("parties_du_discours", "Parties du discours"),
        ("pronoms", "Pronoms"),
        ("pluriels", "Pluriels"),
    )

    SYNTAXE = (
        ("dependances_syntaxiques", "Dépendances syntaxiques"),
        ("structures_passives", "Structures passives"),
        ("structures_syntaxiques", "Structures syntaxiques"),
        ("superlatifs_inferiorite", "Superlatifs inferiorite"),
    )

    SEMANTIQUE = (
        ("adverbiaux_temporels", "Adverbiaux temporels"),
        ("modalite", "Modalité"),
        ("emotions", "Emotions"),
        ("emotyc", "Emotyc"),
        ("connecteurs_organisateurs", "Connecteurs organisateurs"),
        ("entites_nommees", "Entités nommées"),
        ("propositions_subordonnees", "Propositions subordonnées"),
        ("metaphores", "Métaphores"),
    )

    FORMULES_NIVEAU = (
        ("formules_lisibilite_standard","Formules lisibilité standard"),
        ("age", "Age"),
    )

    TEXT_TYPE = (
        ("textarea", 'Texte à copier coller'),
        ("uploadfield", 'Fichiers à déposer'),
    )

    TRAITEMENT_TYPE = (
        ("chaine", 'Chaine synapse'),
        ("semantique", 'Sémantique'),
    )


    #traitement = forms.CharField(label='Type de traitement', widget=forms.RadioSelect(choices=TRAITEMENT_TYPE))
    emailReg = forms.EmailField(label='Email pour la récupération')
    institution = forms.CharField(label='Votre institution')
    levels = forms.MultipleChoiceField(label='Texte ou/et Phrase', widget=forms.CheckboxSelectMultiple, choices=LEVELS)
    processors = forms.MultipleChoiceField(label='Les processeurs...', widget=forms.CheckboxSelectMultiple, choices=PROCESSORS) # widget=forms.CheckboxSelectMultiple
    graphie = forms.MultipleChoiceField(required=False, label='graphie/typographie', widget=forms.CheckboxSelectMultiple, choices=GRAPHIE)
    phonetique = forms.MultipleChoiceField(required=False, label='phonétique/phonologie', widget=forms.CheckboxSelectMultiple, choices=PHONETIQUE)
    lexique = forms.MultipleChoiceField(required=False, label='lexique', widget=forms.CheckboxSelectMultiple, choices=LEXIQUE)
    morphosyntaxe = forms.MultipleChoiceField(required=False, label='morphosyntaxe', widget=forms.CheckboxSelectMultiple, choices=MORPHOSYNTAXE)
    syntaxe = forms.MultipleChoiceField(required=False, label='syntaxe', widget=forms.CheckboxSelectMultiple, choices=SYNTAXE)
    semantique = forms.MultipleChoiceField(required=False, label='sémantique', widget=forms.CheckboxSelectMultiple, choices=SEMANTIQUE)
    formules_niveau = forms.MultipleChoiceField(required=False, label='formules de niveau', widget=forms.CheckboxSelectMultiple, choices=FORMULES_NIVEAU)
    text = forms.CharField(label='Zone de dépôt des textes à analyser', widget=forms.RadioSelect(choices=TEXT_TYPE))
    #file = forms.FileField(label='Fichier à analyser')

class SemanticsForm(forms.Form):
    LEVELS = (
        ('text', 'Texte'),
        ('sentence', 'Phrases'),
    )

    SEMANTIC_PROCESSORS = (
        ("adverbiaux_temporels", "Adverbiaux temporels"),
        ("emotions", "Emotions"),
        ("emotyc", "Emotyc"),
        ("entity_names", "Entités nommées"),
        ("modality", "Modalité"),
        ("metaphores", "Métaphores"),
        ("connecteurs_organisateurs", "Connecteurs organisateurs"),
        ("propositions_subordonnees", "Propositions subordonnées"),
    )

    TEXT_TYPE = (
        ("textarea", 'Texte à copier coller'),
        ("uploadfield", 'Fichiers à déposer'),
    )

    TRAITEMENT_TYPE = (
        ("chaine", 'Chaine synapse'),
        ("semantique", 'Sémantique'),
    )

    #traitement = forms.CharField(label='Type de traitement', widget=forms.RadioSelect(choices=TRAITEMENT_TYPE))
    emailReg = forms.EmailField(label='Email pour la récupération')
    institution = forms.CharField(label='Votre institution')
    levels = forms.MultipleChoiceField(label='Texte ou/et Phrase', widget=forms.CheckboxSelectMultiple, choices=LEVELS)
    processors = forms.MultipleChoiceField(label='Les processeurs...', widget=forms.CheckboxSelectMultiple, choices=SEMANTIC_PROCESSORS) # widget=forms.CheckboxSelectMultiple
    text = forms.CharField(label='Zone de dépôt des textes à analyser', widget=forms.RadioSelect(choices=TEXT_TYPE))
    #file = forms.FileField(label='Fichier à analyser')

class ResultsForm(forms.Form):
    emailCheck = forms.CharField(label='Email de récupération')

#class AdminForm(forms.Form):
    #days = forms.MultipleChoiceField(label='Les processeurs...', choices=DAYS)
