from django.urls import path
from .views import HomePageView
from . import views

urlpatterns = [
    path('chain/', views.chain, name='chain'),
    path('semantics/', views.semantics, name='semantics'),
    path('results/', views.results, name='results'),
    path("results/<str:file_id>", views.download, name="download"),
    path("results/dl/<str:mail>", views.download_all, name="downloadAll"),
    path('check/', views.admin_check, name='admin_check'),
    path('', HomePageView.as_view(), name='home'),
]
