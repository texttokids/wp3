'''
##### This script was tested on google colab with GPU #####
##### Required Packages and Versions #####
Python 3.10.6
scikit-learn==1.2.0
tensorflow==2.10.1
transformers==4.25.1
ktrain==0.35.1
'''

from __future__ import print_function
import argparse
import gzip
import sys
import numpy as np
import pickle as pkl
import os
import math
import tensorflow as tf
from tensorflow.python.client import device_lib

from sklearn.metrics import average_precision_score, mean_absolute_error, mean_squared_error
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, \
    confusion_matrix, classification_report, precision_recall_fscore_support


def get_data(data_path, text_ind, age_ind, start=0):
    texts, ages = [], []
    with open(data_path, 'r') as myfile:
        lines = myfile.readlines()

    nbcols = -1
    ln = 0
    for line in lines[start:]:
        ln = ln + 1
        if line.strip() == "":
            continue
        cols = line.strip().split('\t')
        if nbcols == -1:
            nbcols = len(cols)
        # once the number of columns is noted, it should not change
        if len(cols) != nbcols:
            print(str(ln) + ": nb cols: " + str(len(cols)))
            print(str(ln) + ": cols: " + str(cols))
            continue
        texts.append(cols[text_ind].strip().lower())
        ages.append(float(cols[age_ind].strip()))

    return texts, ages


def evaluate_by_mae_score(data_path, text_ind, age_ind, predictor, start=0):
    txts, ages = get_data(data_path, text_ind, age_ind, start)

    preds = predictor.predict(txts)

    mae = 0.0
    for i in range(len(preds)):
        mae += abs(preds[i] - ages[i])

    # print("Age Prediction MAE Score is:", mae/len(preds))
    return round(mae / len(preds), 2)


def train_age(age_ind, text_ind, train_data_path, dev_data_path, test_data_path, model_save_path, header=""):
    ##### PART 1: Data Loading ######

    # parameters to set

    # column indices in the CSV data file (for me it is tab-separated CSV file)
    # min_age_ind = 1 ?
    # max_age_ind = 2 ?
    # text_ind = 4 ?
    # mean_age_ind = ?? pas necessaire

    # train_data_path = 'Path-To-Train-Data/train.csv'
    # dev_data_path = 'Path-To-Dev-Data/dev.csv'
    # test_data_path = 'Path-To-Test-Data/test.csv'

    # model_save_path = "Path-To-Save-The-Models/trained_models/min_age_predictor_model" # the path to save the model
    # (here the path for min_age as we train the model for min age prediction)

    print("age_ind, text_ind, train_data_path, dev_data_path, test_data_path, model_save_path")
    print(str(age_ind) + " " + str(text_ind) + " " + train_data_path + " " + dev_data_path + " " + test_data_path + " "
          + model_save_path)

    if header == "HEADER":
        start = 1
        print("CSV has a header (HEADER at the last argument of the program call")
    else:
        start = 0
        print("CSV has no header")

    '''
    ### Our training data (sentences) is tokenized and lowercased, but you may try without lowercasing.
    ### For tokenization, you may use spacy python library
    ##### Text Preprocessing (tokenization) #####
    
    # pip install spacy
    # python -m spacy download fr_core_news_md
    
    import spacy
    import fr_core_news_md
    
    nlp = fr_core_news_md.load()
    
    def tokenize_sent(sent):
        doc = nlp(sent)
        sent = [tok.text for tok in doc]
        sent = " ".join(sent)
    
        return sent
    
    '''

    train_texts, train_ages = get_data(train_data_path, text_ind, age_ind, start)
    dev_texts, dev_ages = get_data(dev_data_path, text_ind, age_ind, start)
    test_texts, test_ages = get_data(test_data_path, text_ind, age_ind, start)
    print(len(train_texts), len(dev_texts), len(test_texts))

    ##### PART 2: Training the Model ######

    import ktrain
    from ktrain import text

    model_base = "jplu/tf-camembert-base"
    maxlen = 100
    batch_size = 32

    t = text.Transformer(model_base, maxlen=maxlen)
    train = t.preprocess_train(train_texts, train_ages)
    dev = t.preprocess_test(dev_texts, dev_ages)
    # test = t.preprocess_test(test_texts, test_ages)

    model = t.get_classifier()
    learner = ktrain.get_learner(model, train_data=train, val_data=dev, batch_size=batch_size)

    '''
    # # To decide the learning rate.  
    # # To learn more visit: https://towardsdatascience.com/ktrain-a-lightweight-wrapper-for-keras-to-help-train-neural-networks-82851ba889c
    # learner.lr_find()
    # learner.lr_plot()
    '''

    # # From the plot we decide a learning rate of 1e-5.
    learner.fit_onecycle(1e-5, 3)

    predictor = ktrain.get_predictor(learner.model, t)

    # # To save the trained model as a predictor:
    predictor.save(model_save_path)

    ##### PART 3: Evaluation of the Trained Model ######

    # # To load a saved predictor model:
    # predictor = ktrain.load_predictor(model_save_path)

    mae_dev = evaluate_by_mae_score(dev_data_path, text_ind, age_ind, predictor, start)
    mae_test = evaluate_by_mae_score(test_data_path, text_ind, age_ind, predictor, start)

    print("Age Prediction MAE Score for dev data is:", mae_dev)
    print("Age Prediction MAE Score for test data is:", mae_test)


if __name__ == "__main__":
    if len(sys.argv) < 6:
        print(
            "Usage: python training...py age_index text_index train_data_path dev_data_path test_data_path model_save_path")
        print(str(len(sys.argv)) + " arguments")
        sys.exit(1)
    if len(sys.argv) > 6:
        train_age(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
    else:
        train_age(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5])
