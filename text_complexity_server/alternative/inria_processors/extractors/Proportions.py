import os
import csv
import re

stopwords = ["à", "alors", "au", "aucuns", "aussi", "autre", "avant", "avec", "avoir", "bon", "car", "ce", "cela",
             "ces", "ceux", "chaque", "ci", "comme", "comment", "dans", "des", "du", "dedans", "dehors",
             "depuis", "devrait", "doit", "donc", "début", "elle", "elles", "en", "encore", "essai", "est", "et", "eu",
             "fait", "faites", "fois", "font", "hors", "ici", "il", "ils", "je", "juste", "la", "le", "les",
             "leur", "là", "ma", "maintenant", "mais", "mes", "mien", "moins", "mon", "même", "ni", "notre", "nous",
             "ou", "où", "par", "parce", "pas", "peut", "peu", "plupart", "pour", "pourquoi", "quand",
             "que", "quel", "quelle", "quelles", "quels", "qui", "sa", "sans", "ses", "seulement", "si", "sien", "son",
             "sont", "sous", "soyez", "sujet", "sur", "ta", "tandis", "tellement", "tels", "tes", "ton", "tous",
             "tout", "trop", "très", "tu", "voient", "vont", "votre", "vous", "vu", "ça", "étaient", "état", "étions",
             "été", "être"]

AdvTemp = [" lundi ", " mardi ", " mercredi ", " jeudi ", " vendredi ", " samedi ", " dimanche ",
           " aujourd'hui ", " hier ", " demain ", " avant-hier ", " après-demain ", " maintenant ", " tout à l'heure ",
           " alors ", " après ", " auparavant ", " aussitôt ", " autrefois ", " avant ", " bientôt ", " déjà ",
           " depuis ", " désormais ", " dorénavant ",
           " encore ", " enfin ", " ensuite ", " jadis ", " jamais ", " longtemps ", " maintenant ", " naguère ",
           " parfois ", " puis ", " quelquefois ", " sitôt ",
           " soudain ", " souvent ", " tantôt ", " tard ", " tôt ", " toujours "]

VerbesEtats = ["être", "composer", "former", "constituer", "représenter", "redevenir", "rester", "demeurer", "sembler",
               "paraître"]

ConnecteursLogiques = [["ADDITION",
                 [" et ", " de plus ", " puis ", " en outre ", " non seulement ", " mais encore ", " de surcroît ",
                  " ainsi que ", " également ", " aussi "]],
                ["BUT", [" afin que ", " afin de ", " pour que ", " en vue de ", " en vue que ", " de façon que ",
                         " de façon à "]],
                ["CAUSE",
                 [" car ", " pour ", " en effet ", " effectivement ", " comme ", " par ", " parce que ", " puisque ",
                  " vu que ", " étant donné que ", " grâce à ", " à cause de ", " par suite de ", " eu égard à ",
                  " en raison de ", " du fait que ", " dans la mesure où ", " sous prétexte que ", " compte tenu de "]],
                ["COMPARAISON",
                 [" comme ", " de même que ", " autant que ", " de la même façon que ", " semblablement ",
                  " pareillement ", " plus que ", " moins que ", " selon que ", " suivant que ", " comme si ",
                  " par rapport à "]],
                ["CONCESSION", [" malgré ", " en dépit de ", " quoique ", " bien que ", " alors que ", " même si ",
                                " ce n'est pas que ", " certes ", " bien sûr ", " évidemment ", " il est vrai que ",
                                " toutefois "]],
                ["CONCLUSION",
                 [" en conclusion ", " pour conclure ", " en guise de conclusion ", " en somme ", " bref ", " ainsi ",
                  " donc ", " en résumé ", " en un mot ", " par conséquent ", " finalement ", " enfin ",
                  " en définitive "]],
                ["CONDITION",
                 [" si ", " au cas où ", " à condition que ", " pourvu que ", " à moins que ", " en admettant que ",
                  " pour peu que ", " à supposer que ", " en supposant que ", " dans l'hypothèse où ",
                  " dans le cas où "]],
                ["CONSEQUENCE",
                 [" donc ", " aussi ", " partant ", " alors ", " ainsi ", " ainsi donc ", " par conséquent ",
                  " de ce fait ", " si bien que ", " d'où ", " en conséquence ", " conséquemment ", " par suite ",
                  " c'est pourquoi ", " de sorte que ", " en sorte que ", " de façon que ", " de manière que",
                  " si bien que ", " tant et si bien que"]],
                ["ENUMERATION",
                 [" d'abord ", " tout d'abord ", " de prime abord ", " en premier lieu ", " premièrement",
                  " en deuxième lieu ", " en second lieu ", " deuxièmement ", " après ", " ensuite ", " de plus ",
                  " quant à ", " en troisième lieu ", " puis ", " en dernier lieu ", " pour conclure ", " enfin "]],
                ["EXPLICATION", [" à savoir ", " c'est-à-dire "]],
                ["ILLUSTRATION",
                 [" par exemple ", " comme ", " c'est ainsi que ", " c'est le cas de ", " notamment ", " entre autres ",
                  " en particulier ", " à l'image de ", " comme l'illustre ", " comme le souligne ", " tel que "]],
                ["JUSTIFICATION",
                 [" car ", " c'est-à-dire ", " en effet ", " parce que ", " puisque ", " ainsi ", " c'est ainsi que ",
                  " du fait de "]],
                ["OPPOSITION",
                 [" mais ", " cependant ", " or ", " en revanche ", " alors que ", " pourtant ", " par contre ",
                  " tandis que ", " néanmoins ", " au contraire ", " pour sa part ", " d'un autre côté ",
                  " en dépit de ", " malgré ", " nonobstant ", " au lieu de "]],
                ["RESTRICTION",
                 [" cependant ", " toutefois ", " néanmoins ", " pourtant ", " mis à part ", " en dehors de ",
                  " hormis ", " à défaut de ", " excepté ", " sauf ", " uniquement ", " simplement "]],
                ["EXCLUSION", [" hormis ", " sauf que ", " excepté que "]],
                ["TEMPS",
                 [" quand ", " lorsque ", " avant que ", " après que ", " alors que ", " dès lors que ", " depuis que ",
                  " tandis que ", " en même temps que ", " pendant que ", " au moment où "]]]


def ListageDesmots(Phrase):
    Mots = []
    for j in range(len(Phrase)):
        mot = (Phrase[j])
        if mot.get("upos") != "PUNCT":  # upostag, PONCT
            Mots += [mot.get("lemma", mot.get("text"))]  # form
    return Mots


def ProportionsAdvTemp(phrase):
    phraseSimple = ""
    nbmots = 0
    for i in range(len(phrase)):
        mot = phrase[i]
        if mot.get('upos') != 'PUNCT':
            phraseSimple += " " + mot.get('lemma', mot.get("text")) + " "
            nbmots += 1
    if nbmots > 0:
        phraseSimple.lower()
        phraseSimple = re.sub(' +', ' ', phraseSimple)
        NbAdvTemp = sum(phraseSimple.count(k) for k in AdvTemp)
        return [NbAdvTemp / nbmots]
    else:
        print("ERROR : phrase sans mot !")
        return [0]


def ProportionsVerbesdEtat(phrase):
    ListOfLemmes = ""
    nbmots = 0
    for i in range(len(phrase)):
        verbe = phrase[i]
        if verbe.get('upos') == 'VERB' and 'aux' not in verbe.get('deprel'):  # les verbes non auxiliaires  aux_tps
            ListOfLemmes += " " + verbe.get('lemma') + " "
            nbmots += 1
        else:
            if verbe.get('upos') != 'PUNCT':
                nbmots += 1
    if ListOfLemmes != "":
        ListOfLemmes.lower()
        NbVerbesEtat = sum(ListOfLemmes.count(k) for k in VerbesEtats)
        return [NbVerbesEtat / nbmots]
    else:  # Cas où il n'y a pas de verbes
        return [0]


def ProportionsConnLogiques(phrase):
    phraseSimple = ""
    nbmots = 0
    ListOfProp = []
    for i in range(len(phrase)):
        mot = phrase[i]
        if mot.get('upos') != 'PUNCT':
            phraseSimple += " " + mot.get('lemma', mot.get("text")) + " "
            nbmots += 1
                        
    if nbmots > 0:
        phraseSimple.lower()
        phraseSimple = re.sub(' +', ' ', phraseSimple)
        
        for j in range(len(ConnecteursLogiques)):
            NbCurrentCat = sum(phraseSimple.count(k) for k in ConnecteursLogiques[j][1])
            ListOfProp += [NbCurrentCat / nbmots]
        return ListOfProp
    else:
        print("ERROR : phrase sans mot !")
        #return [0]
        for j in range(len(ConnecteursLogiques)):
            ListOfProp += [0]
        return ListOfProp        


def StopWordsProp(Phrase):
    ListMots = ListageDesmots(Phrase)
    StopWords = 0
    for i in range(len(ListMots)):
        if ListMots[i] in stopwords:
            StopWords += 1
    return StopWords / len(ListMots) * 100


def Proportions(Phrase):
    Clitiques = 0
    Verbes = 0
    Noms = 0
    ADJ = 0
    Stopwords = 0
    PONCT = 0
    nbmots = len(Phrase)
    lenmot = 0
    for j in range(len(Phrase)):
        mot = (Phrase[j])
        lenmot += len(mot.get("lemma", mot.get("text")))
        if mot.get("upos") == "PC":  # CL #no equivalent in Stanza pipeline
            Clitiques += 1
        if mot.get('upos') == 'VERB':
            Verbes += 1
        if mot.get('upos') == 'NOUN':
            Noms += 1
        if (mot.get('lemma') in stopwords) or (mot.get('upos') == "DET"):  # D
            Stopwords += 1
        if mot.get('upos') == 'ADJ':
            ADJ += 1
        if mot.get('upos') == 'PUNCT':
            nbmots -= 1
            PONCT += 1
    if nbmots == 0:
        print('ERROR: Pas de mot dans la phrase')
        return [
            # 0, #proportion_verbe
            # 0, #proportion_nom
            # 0, #proportion_adjectif
            # 0, #proportion_clitique
            0#, #proportion_stopWords
            # 0, #proportion_symbole
            # 0  #rapport_ponctuation_sur_mots
            ]
    else:
        return ([
            # Verbes / nbmots * 100, 
            # Noms / nbmots * 100, 
            # ADJ / nbmots * 100, 
            # Clitiques / nbmots * 100, 
            Stopwords / nbmots * 100, 
            # lenmot / nbmots,
            # PONCT / nbmots * 100
            ])


def ListageDesLemmes(Phrase):
    Lemmes = []
    for j in range(len(Phrase)):
        mot = (Phrase[j])
        if mot.get("upos") != "PUNCT":
            Lemmes += [mot.get("lemma", mot.get("text"))]
    return Lemmes


def DiversiteDesLemmes(Phrase):
    listeLemmes = ListageDesLemmes(Phrase)
    listeLemmesDiff = []
    for i in range(len(listeLemmes)):
        if listeLemmes[i] not in listeLemmesDiff:
            listeLemmesDiff += [listeLemmes[i]]
    if len(listeLemmes)>0: 
        return len(listeLemmesDiff) / len(listeLemmes) * 100
    else:
        return 0


def CsvMakerPhrases(ListOfPhrases):
    ListOfConnLogiquesCatProp = []
    for k in range(len(ConnecteursLogiques)):
        ListOfConnLogiquesCatProp += ["Prop" + ConnecteursLogiques[k][0].capitalize()]
    ListfeaturesPhrases = ['DiversitéLemmes', 'ProportionsVerbes', 'ProportionNoms', 'ProportionsAdjectif',
                           #'ProportionClitiques', 
                           'ProportionStopWords', 'ProportionSymboles',
                           'RapportPonctuationSurMots', 'ProportionAdvTemp',
                           'ProportionVerbeEtat'] + ListOfConnLogiquesCatProp
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [
            [DiversiteDesLemmes(phrase)] + Proportions(phrase) + ProportionsAdvTemp(phrase) + ProportionsVerbesdEtat(
                phrase) + ProportionsConnLogiques(phrase)]
    if 'ProportionsFeatures.csv' not in os.listdir(os.getcwd()):
        with open('ProportionsFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('ProportionsFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for j in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[j])
    return 'ProportionsFeatures.csv'
