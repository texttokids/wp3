from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document


processor_name = 'words_util'

features_list = ['words_list', 'words_count']

def setFeatures(input):
    words_list = []
    
    if isinstance(input, Document):
        for sentence in input.sentences:
            words_list += [word for word in sentence.words if word.upos != "PUNCT"]
    else:
        words_list = [word for word in input.words if word.upos != "PUNCT"]
        

    words_features = [words_list,len(words_list)]

    
    features = zip(features_list, words_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class WordsUtilProcessor(Processor):
    """ Processor that counts words """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            setFeatures(doc)

        if "sentence" in self.levels:
            for sentence in doc.sentences:
                setFeatures(sentence)                                          

        return doc
