assez
aucun
aucune
autant
autre
autre chose
autrui
beaucoup
bon nombre
certain
certaine
certaines
certains
chacun
chacune
combien
d'aucuns
d'aucunes
d'autre
d'autres
davantage
des autres
Dieu sait laquelle
Dieu sait lequel
Dieu sait lesquelles
Dieu sait lesquels
Dieu sait où
Dieu sait qui
Dieu sait quoi
grand-chose
guère
je ne sais combien
je ne sais laquelle
je ne sais lequel
je ne sais lesquelles
je ne sais lesquels
je ne sais qui
je ne sais quoi
l'autre
l'un
l'un et l'autre
l'un ou l'autre
l'autre
l'une
l'une et l'autre
l'une ou l'autre
la même
la plupart
le même
les autres
les mêmes
les unes
les unes et les autres
les unes ou les autres
les uns
les uns et les autres
les uns ou les autres
maintes
maints
n'importe lequel
n'importe qui
n'importe quoi
n'importe laquelle
n'importe lesquelles
n'importe lesquels
n'importe où
nul
nulle
on
on ne sait laquelle
on ne sait lequel
on ne sait lesquelles
on ne sait lesquels
on ne sait où
on ne sait qui
on ne sait quoi
où que ce soit
pas un
pas une
personne
peu
peu de chose
plus d'un
plus d'une
plusieurs
quelqu'un
quelqu'une
quelque chose
quelques-unes
quelques-uns
qui
qui de droit
qui que ce soit
quiconque
quoi que ce soit
rien
tant
tel
telle
telles
tels
tout
tout ceci
tout cela
tout le monde
toute
trop
un
un autre
un tel
une
une autre
une telle
