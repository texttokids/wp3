from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from inria_processors.extractors.FrequenceOccurence import *
from tools.processor_export import processor_export_format

processor_name = 'richesse_lexicale'

features_list = [
    "moyenne_probabilite_mots", 
    "variance_probabilite_mots",
    "diversite_lemmes"]

features_list_sentence = ['nombre_mots'] + features_list
features_list_text = ["nombre_moyen_mots"] + features_list

def setFeatures(input):

    #conll format conversion
    if isinstance(input, Document):
        conll_text = input.to_dict()
    else:
        conll_text = [input.to_dict()]
    
    #flatten list of sentences if input is doc
    if isinstance(conll_text, list):
        conll_text = [item for sublist in conll_text for item in sublist]

    extracted_features = GlobalisationFrequenceOccurence(conll_text)
    features = zip(["moyenne_probabilite_mots", 
                    "variance_probabilite_mots"], 
                    extracted_features)
    for f, s in features:
        setattr(input, f, s)
    
    extracted_lemma_diversity_feature = extract_lemma_diversity_feature(input)
    setattr(input, "diversite_lemmes", extracted_lemma_diversity_feature)


def extract_lemma_diversity_feature(input):
    lemmas = set()

    if input.words_count>0:
        return len(lemmas)/input.words_count
    else:
        return 0



@register_processor(processor_name)
class LexicalRichnessProcessor(Processor):
    """ Processor that extracts lexical richness metrics at text and sentence levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list_sentence)

        #sentence level analysis            
        for idx, sentence in enumerate(doc.sentences):   
            setattr(sentence, 'nombre_mots', sentence.words_count)
            setFeatures(sentence)


        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list_text)
            setattr(doc, "nombre_moyen_mots", (doc.num_words/len(doc.sentences)) if (len(doc.sentences) > 0) else 0)
            setFeatures(doc)

        return doc
