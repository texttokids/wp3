from stanza.pipeline.processor import Processor, register_processor
from lexicalrichness import LexicalRichness
from nltk.tokenize import word_tokenize
from tools.processor_export import processor_export_format

processor_name = 'a_tester_diversite_lexicale'

features_list = ["nombre_mots",
        "nombre_termes_uniques",
        "ratio_chotlos",
        "ratio_guiraud",
        "ratio_carrol",
        "ratio_herdan",
        "ratio_dugast"
        "ratio_maas",
        "mean_segmental_ratio_chatlos", # inspect.getdoc(LexicalRichness.msttr).split('\n')[0],
        "moving_average_ratio_chatlos", # inspect.getdoc(LexicalRichness.mattr).split('\n')[0],
        "textual_diversity", # inspect.getdoc(LexicalRichness.mtld).split('\n')[0],
        "hypergeometric_distribution_diversity" # inspect.getdoc(LexicalRichness.hdd).split('\n')[0],
        ]


def setFeatures(input):
    """
    Computes a set of lexical richness features and measures.
    Statistics about the text content.
    :param text: str
    :return: dict, list
    """
    tokenized_text = word_tokenize(input.text, language='french')
    lex = LexicalRichness(' '.join(tokenized_text))

    word_count = getattr(lex, "words")

    if word_count>2:
        
        f = ["words", "terms", "ttr", "rttr", "cttr", "Herdan", "Summer", "Dugast", "Maas"]
        data = [lex.msttr(segment_window=2),
                lex.mattr(window_size=2),
                lex.mtld(threshold=0.72),
                lex.hdd(draws=2)]

        values = []
        for fn in f:
            try:
                v = getattr(lex, fn)
            except:
                v = 0
            values.append(v)
        values = values + data
        # return dict(zip(def_, values)), lex.wordlist
        lexical_richness_dict = dict(zip(features_list, values))

    else:
        # if input sequence contains less than two tokens, lexical richness can not be computed properly : 
        # all values are therefore by default set to one
        lexical_richness_dict = dict(zip(features_list,[1,1,1,1,1,1,1,1,1,1,1,1,1]))
    
    for fp, f in lexical_richness_dict.items():
        setattr(input, fp, f)        
        


@register_processor(processor_name)
class LexicalRichness2Processor(Processor):
    """ Processor that computes lexical richness at text and sentence levels """
    _provides = {processor_name}
    OVERRIDE = True

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")        
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)   

        return doc
