# from inria_processors.extractors.Dependances import *
from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'superlatifs_inferiorite'

features_list = [
    'nombre_superlatifs_inferiorite',
    'proportion_superlatifs_inferiorite',]

def setFeatures(input):

    is_superlative = False
    superlative_count = 0

    for word in input.words_list :
        
        if word.lemma == "moins":
            is_superlative = True
        elif is_superlative: 
            is_superlative = False
            if word.upos == 'ADJ':
                superlative_count += 1

    proportion = 0
    if input.words_count>0:
        proportion = superlative_count/input.words_count
        
    extracted_features = [superlative_count, proportion]    
        
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class InferioritySuperlativeProcessor(Processor):
    """ Processor that computes inferiority superlatives features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                          

        return doc
