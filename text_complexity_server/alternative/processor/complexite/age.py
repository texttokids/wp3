from inria_processors.extractors.Dependances import *
from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from nltk import word_tokenize
from api import settings
import ktrain

import os

processor_name = "age"
features_list = ['age_min','age_max','age_moyen']

if os.name == 'nt':
    min_age_model_path = os.path.abspath(os.path.join(settings.SENTENCE_LEVEL_AGE_MODEL_PATH, "min_age_predictor_model/")) #"resources/age_level_models/min_age_predictor_model/"
    max_age_model_path = os.path.abspath(os.path.join(settings.SENTENCE_LEVEL_AGE_MODEL_PATH, "max_age_predictor_model/")) #"resources/age_level_models/max_age_predictor_model/"
else:
    min_age_model_path = settings.SENTENCE_LEVEL_AGE_MODEL_PATH+"/min_age_predictor_model/" #"resources/age_level_models/min_age_predictor_model/"
    max_age_model_path = settings.SENTENCE_LEVEL_AGE_MODEL_PATH+"/max_age_predictor_model/" #"resources/age_level_models/max_age_predictor_model/"

min_age_level_predictor = ktrain.load_predictor(min_age_model_path)
max_age_level_predictor = ktrain.load_predictor(max_age_model_path)
print('age_level_models loaded') 

def setFeatures(input):
     
    sentence = ' '.join(word_tokenize(input.text, language="french")).lower()
    
    # print('predicting age_level min')
    min_age_prediction = min_age_level_predictor.predict(sentence)
    # print('predicting age_level max') 
    max_age_prediction = max_age_level_predictor.predict(sentence)

    mean_age_prediction = (min_age_prediction+max_age_prediction)/2

    age_predictions = [min_age_prediction, max_age_prediction, mean_age_prediction]
    extracted_features = []

    for age in age_predictions:                                                                                                                                                              
        extracted_features.append("{:.2f}".format(age)) 
    
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    

    return age_predictions

@register_processor("age")
class AgeProcessor(Processor):
    """ Processor that predicts target age min and max at sentence and text levels """
    _provides = {'age'}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")        
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):


        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            
        sum_age_min = sum_age_max = sum_age_moyen = 0

        for sentence in doc.sentences:
            age_min, age_max, age_moyen = setFeatures(sentence)
            sum_age_min += age_min
            sum_age_max += age_max
            sum_age_moyen += age_moyen

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            sentences_count = len(doc.sentences)

            if sentences_count > 0:
                age_predictions = [sum_age_min/sentences_count, sum_age_max/sentences_count, sum_age_moyen/sentences_count]

                extracted_features = []

                for age in age_predictions:
                    extracted_features.append("{:.2f}".format(age))

                features = zip(features_list, extracted_features)
                for f, s in features:
                    setattr(doc, f, s)


        return doc
