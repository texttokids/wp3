import pandas as pd
import os


class processor_export_format(object):
    def __init__(self, name, format, features):
        pass

    """
        Configure the exports.
        :param name: str / name of the export file
        :param export_level: str / level where the data is stored : "word" or "sentence"
        :param format: str / format export : "csv"
        :param features: list / list of attributes that will be used as columns 
        :return: none
    """

    def add_config(doc, name, export_level, format, features):
        if not hasattr(doc, 'custom_processor'):
            setattr(doc, 'custom_processor', [])

        export = export_param(name, export_level, format, features)
        doc.custom_processor.append(export)

    def export(doc, prefix="", out="temp", write_file=False):
        dfs = {}
        if write_file and not os.path.exists(out):
            os.mkdir(out)
        for export_config in doc.custom_processor:
            if export_config.format == "csv":
                if export_config.level == "text":
                    columns = ["text_id"] + export_config.features
                    lines = [
                        [0] + [getattr(doc,
                                               feature,
                                               "") for feature in export_config.features
                                       ] 
                    ]
                    df_txtsts = pd.DataFrame(columns=columns, data=lines)
                    if write_file:
                        df_txtsts.to_csv(f"{out}/{prefix}_{export_config.name}.csv", encoding='utf8')

                if export_config.level == "sentence":
                    columns = ["sent_id", "sentence"] + export_config.features
                    lines = [
                        [idx,
                         sent.text] + [getattr(sent,
                                               feature,
                                               "") for feature in export_config.features
                                       ] for idx, sent in enumerate(doc.sentences)
                    ]
                    df_txtsts = pd.DataFrame(columns=columns, data=lines)
                    if write_file:
                        df_txtsts.to_csv(f"{out}/{prefix}_{export_config.name}.csv", encoding='utf8')

                if export_config.level == "word":
                    columns = ["word_id", "sentence", "word"] + export_config.features
                    lines = []
                    for idx, sent in enumerate(doc.sentences):
                        lines += [
                            [int(str(idx) + str(word.id)),
                             sent.text,
                             word.text] + [getattr(word, feature, "") for feature in export_config.features]
                            for word in sent.words]
                    df_txtsts = pd.DataFrame(columns=columns, data=lines)
                    if write_file:
                        df_txtsts.to_csv(f"{out}/{prefix}_{export_config.name}.csv", encoding='utf8')
                dfs[export_config.name] = df_txtsts
        return dfs


class export_param(object):
    def __init__(self, name, level, format, features):
        self.name = name
        self.format = format
        self.features = features
        self.level = level
