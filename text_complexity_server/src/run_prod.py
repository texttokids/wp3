import logging.config
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"

from flask import Flask, Blueprint
from api import settings

import sys
sys.path.append("services/descriptor")
sys.path.append("inria_processors")

from api.descriptors_extractor.extraction_ws import text_complexity_linguistic_description_namespace
from api.flaskapi import api
from waitress import serve

app = Flask(__name__)  # the web service name
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)


def configure_app_prod(flask_app):
    """
    Set the whole configuration of the flask_app application
    :param flask_app: obj, Flask object
    :return:
    """
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP


def initialize_app(flask_app):
    """
    Init the flask_app by defining the namespace.
    :param flask_app: obj, Flask object
    :return:
    """
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)

    # add the input documents
    api.add_namespace(text_complexity_linguistic_description_namespace)
    flask_app.register_blueprint(blueprint)


def main():
    # 1. Configure the app
    configure_app_prod(app)

    # 2. Init the WS
    initialize_app(app)

    # 3. logging status
    log.info('>>>>> Starting server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))

    # 4. Run the application
    serve(app, host='0.0.0.0', port=8081, threads=1)


if __name__ == "__main__":
    main()