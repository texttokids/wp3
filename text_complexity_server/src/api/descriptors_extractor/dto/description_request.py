from flask_restx import Model, fields

# Flask model
description_request = Model('description_request', {
    'text': fields.String,
    'levels': fields.List(fields.String),
    'processors': fields.List(fields.String)
})