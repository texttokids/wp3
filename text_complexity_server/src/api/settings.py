# Flask settings
FLASK_SERVER_NAME = 'localhost:8081'
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = True

# Sentence age prediction model path
SENTENCE_LEVEL_AGE_MODEL_PATH = '../age_model/models_for_fiction_data_TALN22_v3'
#SENTENCE_LEVEL_AGE_MODEL_PATH = '../age_model/models_for_children'
