from flask_restx import reqparse

word_arguments = reqparse.RequestParser()
word_arguments.add_argument('word',
                            type=str,
                            required=False,
                            default="world",
                            help='The people to say hello')
