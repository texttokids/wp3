import logging

from flask import request
from flask_restx import Resource
from api.flaskapi import api
from api.hello_world.dto.word import word_arguments

log = logging.getLogger(__name__)

hello_world_namespace = api.namespace('api/hello_world',
                   description='Operations related to tests of concept discovery service')


@hello_world_namespace.route('/')
class HelloWorldService(Resource):

    @staticmethod
    def get():
        return 'Hello world!'

    @api.expect(word_arguments)
    def post(self):
        args = word_arguments.parse_args(request)
        word = args.get('word')
        return 'Hello ' + word + '!'
