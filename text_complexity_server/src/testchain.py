# testchain.py
# direct call of texttokids main chain
import os
import sys

def set_local_path():
    localcurdir = os.getcwd()
    print("testchain.py (text_complexity_server/src)", localcurdir)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("this file", dir_path)
    os.chdir(dir_path)
    # sys.path.insert(1, dir_path)
    return localcurdir

def read_whole_file(name):
    # Open a file: file
    file = open(name, mode='r')
    # read all lines at once
    all_of_it = file.read()
    # close the file
    file.close()
    return all_of_it


if __name__ == "__main__":
    locpth = set_local_path()
    from api.descriptors_extractor.extraction_ws import process_chain

    print(os.getcwd())
    if len(sys.argv) != 4:
        print('usage: testchain.py text processor level')
        sys.exit(1)
    if sys.argv[1][0] == "@":
        mytext = read_whole_file(sys.argv[1][1:])
    else:
        mytext = sys.argv[1]
    mylevel = sys.argv[2].split(",")
    myproc = sys.argv[3].split(",")
    print(process_chain(mytext, mylevel, myproc))

    os.chdir(locpth)
