from stanza.pipeline.processor import Processor, register_processor
import textstat
from tools.processor_export import processor_export_format


processor_name = 'formules_lisibilite_standard'

features_list = ["flesch_reading_ease",
            "smog_index",
            "flesch_kincaid_grade",
            "coleman_liau_index",
            "automated_readability_index",
            "dale_chall_readability_score",
            "linsear_write_formula",
            "french_standard"]
            
switcher = {
    '-1th and 0th grade': "maternelle",
    '0th and 1st grade': "maternelle-CP",
    '1st and 2nt grade': "CP-CE1",
    '2nd and 3rd grade': "CE1-CE2",
    '3rd and 4th grade': "CE2-CM1",
    '4th and 5th grade': "CM1-CM2",
    '5th and 6th grade': "CM2-6e",
    '6th and 7th grade': "6e-5e",
    '7th and 8th grade': "5e-4e",
    '8th and 9th grade': "4e-3e",
    '9th and 10th grade': "3e-2nde",
    '10th and 11th grade': "2nde-1e",
    '11th and 12th grade':"1e-terminale"
}


def french_standard_level_mapping(text):

    text_standard = textstat.text_standard(text)
    return switcher.get(text_standard, "superieur")


functions = [textstat.flesch_reading_ease,
             textstat.smog_index,
             textstat.flesch_kincaid_grade,
             textstat.coleman_liau_index,
             textstat.automated_readability_index,
             textstat.dale_chall_readability_score,
             textstat.linsear_write_formula,
             french_standard_level_mapping]

def setFeatures(input):
    for fn, f in zip(features_list, functions):
        setattr(input, fn, f(input.text))


@register_processor(processor_name)
class ReadabilityProcessor(Processor):
    """ Processor that computes readability scores """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)   

        return doc
