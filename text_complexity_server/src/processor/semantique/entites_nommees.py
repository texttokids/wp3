from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

processor_name = 'entites_nommees'

raw_features_list = [
    "entites_nommees",
    "personnes",
	"lieux",
	"organisations",
    "autres_entites"
]
features_list = ["proportion_" + feature for feature in raw_features_list]

def setFeatures(input):

    raw_features_dict = extract_raw_features(input)
    computed_features_dict = dict.fromkeys(features_list , 0)

    named_entities_proportion = get_proportion(raw_features_dict['entites_nommees'], input.words_count)
    computed_features_dict['proportion_entites_nommees'] = named_entities_proportion

    person_proportion = get_proportion(raw_features_dict['personnes'], input.words_count)
    computed_features_dict['proportion_personnes'] = person_proportion

    location_proportion = get_proportion(raw_features_dict['lieux'], input.words_count)
    computed_features_dict['proportion_lieux'] = location_proportion

    organization_proportion = get_proportion(raw_features_dict['organisations'], input.words_count)
    computed_features_dict['proportion_organisations'] = organization_proportion

    other_entities_proportion = get_proportion(raw_features_dict['autres_entites'], input.words_count)
    computed_features_dict['proportion_autres_entites'] = other_entities_proportion

    for f, s in computed_features_dict.items():
        setattr(input, f, s)  


def extract_raw_features(input):
    raw_features_dict = dict.fromkeys(raw_features_list , 0)

    for entity in input.entities:
        if entity.type == "PER":
            raw_features_dict['personnes']+=len(entity.text.split(' '))
        elif entity.type == "ORG":
            raw_features_dict['organisations']+=len(entity.text.split(' '))
        elif entity.type == "LOC":
            raw_features_dict['lieux']+=len(entity.text.split(' '))
        elif entity.type == "MISC":
            raw_features_dict['autres_entites']+=len(entity.text.split(' '))
    
        raw_features_dict['entites_nommees'] += len(entity.text.split(' '))

    return raw_features_dict     



def get_proportion(feature1, feature2):
    if feature2==0:
        return 0
    else:
        return feature1/feature2


@register_processor(processor_name)
class NamedEntityRecognitionProcessor(Processor):
    """ Processor that recognizes named entites at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        #text level analysis
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        #sentence level analysis
        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)

        return doc
