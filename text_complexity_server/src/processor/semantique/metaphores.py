from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from stanza.models.common.doc import Document
import os
import re
from pathlib import Path
import stanza


processor_name = 'metaphores'

directory = "processor/semantique/metaphores/"
metaphores_filename = "metaphores_min_2_mots.txt"
metaphores_lemmatized_filename = "metaphores_min_2_mots_lemmatises.txt"


def get_lemma(input):
    lemmatized_text = ""

    if isinstance(input, Document):
        for sentence in input.sentences:
            if len(sentence.words) > 1:
                for word in sentence.words:
                    if word.lemma is not None:
                        lemmatized_text += word.lemma + " "
    else:
        if len(input.words) > 1:
            for word in input.words:
                if word.lemma is not None:
                    lemmatized_text += word.lemma + " "
    return lemmatized_text.rstrip() 


def load_features_dictionary(directory):
    features_dictionary = {}

    feature = Path(metaphores_filename).stem

    f = os.path.join(directory, metaphores_filename)

    f_path_lemmatized = os.path.join(directory, metaphores_lemmatized_filename)
    
    if os.path.isfile(f_path_lemmatized):
        print("metaphores lemmatized file loading")
        with open(f_path_lemmatized, 'r', encoding="utf-8") as file:
            for line in file:  
                entry = line.rstrip()
                if entry != "":
                    if feature in features_dictionary:
                        features_dictionary[feature].append(entry)
                    else:
                        features_dictionary[feature] = [entry]            
    
    # if metaphore lemmatized file does not exist, cretae it
    elif os.path.isfile(f):
        f_lemmatized = open(f_path_lemmatized, 'w', encoding="utf-8")
        
        from stanza.pipeline.core import DownloadMethod
        nlp_processor = stanza.Pipeline('fr',
                        processors="tokenize,lemma",
                        use_gpu=False,
                        pos_batch_size=3000,
                        download_method=DownloadMethod.REUSE_RESOURCES)

        with open(f, 'r', encoding="utf-8") as file:
            for line in file:  

                entry = get_lemma(nlp_processor(line.rstrip()))
                if entry != "":
                    f_lemmatized.write(entry)
                    f_lemmatized.write('\n')

                    if feature in features_dictionary:
                        features_dictionary[feature].append(entry)
                    else:
                        features_dictionary[feature] = [entry]
        f_lemmatized.close() 
   
    return features_dictionary

features_dictionary = load_features_dictionary(directory)


def set_feature_list():
    f_list = []
    features = list(features_dictionary.keys())
    for feature in features:
        f_list.append("nombre_"+feature)
        f_list.append("proportion_"+feature)
    return f_list
    
features_list = set_feature_list()


def setFeatures(input):

    text = ""
    sentence_count = 0
    
    #lemmatize input
    if isinstance(input, Document):
        for sentence in input.sentences:
            text += get_lemma(sentence)
            sentence_count += 1
    else:
        text = get_lemma(input)
        sentence_count = 1
    
    extracted_features = []

    for feature in list(features_dictionary.keys()):
        extracted_feature = extract_feature(feature, text)
        extracted_features.append(extracted_feature)
        extracted_features.append((extracted_feature/sentence_count) if sentence_count > 0 else 0)

    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


def extract_feature(feature, text):
    feature_count = 0
    feature_values = features_dictionary[feature]

    for feature_value in feature_values: 
        feature_count += len(re.findall(feature_value, text))

    return feature_count


@register_processor(processor_name)
class LogicalConnectorOrganizersProcessor(Processor):
    """ Processor that computes metaphores proportion at sentence and text level """
    _requires = {"tokenize,lemma"}
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)   

        return doc

