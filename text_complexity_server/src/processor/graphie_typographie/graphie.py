from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

processor_name = "graphie"
features_list = [   'frequence_lettres_moyenne', # Moyenne des fréquences des lettres
                    'variance_frequence_lettres', # Écart-type de la moyenne des fréquences des lettres
                    'longueur_mots_moyenne', # Moyenne de la longueur des mots en nombre de lettres
                    'variance_longueur_mots'] # Écart-type de la longueur des mots en nombre de lettres

switcher = {
    'e': 12.10, 'a': 7.11, 'i': 6.59, 's': 6.51, 'n': 6.39, 'r': 6.07, 't': 5.92, 'o': 5.02, 'l': 4.96, 'u': 4.49,
    'd': 3.67, 'c': 3.18, 'm': 2.62, 'p': 2.49,
    'é': 1.94, 'g': 1.23, 'b': 1.14, 'v': 1.11, 'h': 1.11, 'f': 1.11, 'q': 0.65, 'y': 0.46, 'x': 0.38, 'j': 0.34,
    'è': 0.31, 'à': 0.31, 'k': 0.29, 'w': 0.17,
    'z': 0.15, 'ê': 0.08, 'ç': 0.06, 'ô': 0.04, 'â': 0.03, 'î': 0.03, 'û': 0.02, 'ù': 0.02, 'ï': 0.01, 'ü': 0.01,
    'ë': 0.01, 'ö': 0.01, 'í': 0.01,
}

def letter_frequency(letter):
    return switcher.get(letter, 0)  # On suppose que les autres caractères sont trop rares, y compris les majuscules


def get_word_form_features(input):

    mean_frequency = 0
    frequency_variance = 0
    mean_words_length = 0
    word_length_variance = 0

    if input.words_count > 0:

        input_letters_frequency = []
        total_word_length_list = []
        letters_frequency = 0
        letters_count_dict = {}
        
        for word in input.words_list:
            total_word_length_list += [len(word.text)]
            for letter in word.text:
                letters_count_dict[letter] = letters_count_dict.get(letter,0) + 1

        for letter in letters_count_dict:
            letters_frequency += letter_frequency(letter) * letters_count_dict[letter]            

        total_word_length = len(input.words_list)
        
        input_letters_frequency = letters_frequency / len(total_word_length_list)
        mean_frequency = input_letters_frequency / total_word_length
        frequency_variance = (input_letters_frequency ** 2 - mean_frequency ** 2) / total_word_length
        
        mean_words_length = sum(total_word_length_list) / total_word_length
        word_length_variance = (total_word_length ** 2 - mean_words_length ** 2) / total_word_length

    return [mean_frequency,frequency_variance,mean_words_length,word_length_variance]


def setFeatures(input):

    extracted_features = get_word_form_features(input)
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)

@register_processor(processor_name)
class CharacterProcessor(Processor):
    """ Processor that computes character frequencies at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")        
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)  

        return doc
