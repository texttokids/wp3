from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from inria_processors.extractors import Temps_composes
from tools.processor_export import processor_export_format
from inria_processors.extractors.Proportion_de_conjugaisons import *

processor_name = 'flexions_verbales'

tenses_proportion_features = [
                    'proportion_ind-pst', 
                    'proportion_ind-ps',
                    'proportion_ind-fut', 
                    'proportion_ind-impft',
                    'proportion_subj-pst', 
                    'proportion_ind-cond',
                    'proportion_inf-',
                    'proportion_ind-passe_comp', 
                    'proportion_ind-passe_ant',
                    'proportion_ind-fut_ant', 
                    'proportion_ind-plus_que_parf',
                    'proportion_subj-passe', 
                    'proportion_cond-passe',
                    'proportion_inf_passe']

mode_proportion_features = [
                    'proportion_indicatif', 
                    'proportion_subjonctif', 
                    'proportion_infinitif']

flexions_proportion_features = [
    'proportion_flexions_1e_personne', 
    'proportion_flexions_2e_personne', 
    'proportion_flexions_3e_personne',
    'proportion_flexions_singulier', 
    'proportion_flexions_pluriel']

simple_or_composed_features = ['proportion_temps_compose', 'proportion_temps_simple']

tense_diversity_feature = ['diversite_temps_verbaux'] 
tense_system_diversity_feature = ['diversite_systemes_temps'] 
tense_features = ['passe', 'present', 'futur']

features_list = tense_diversity_feature + tenses_proportion_features + tense_system_diversity_feature + tense_features + simple_or_composed_features + mode_proportion_features + flexions_proportion_features


def setFeatures(input):

    #conll format conversion
    if isinstance(input, Document):
        conll_text = input.to_dict()
    else:
        conll_text = [input.to_dict()]
    
    #flatten list of sentences if input is doc
    if isinstance(conll_text, list):
        conll_text = [item for sublist in conll_text for item in sublist]

    setattr(input, features_list[0], len(Temps_composes.DiversiteDesTemps(conll_text)))

    extracted_tenses_proportion_features = zip(tenses_proportion_features,
                            Temps_composes.matchingPropsTemps(Temps_composes.ProportionTemps(conll_text)))
    for f, s in extracted_tenses_proportion_features:
        setattr(input, f, s)

    setattr(input, 'diversite_systemes_temps', len(Temps_composes.DiversiteDesSysTemp(conll_text)))

    extracted_tense_features = zip(tense_features,
                            Temps_composes.matchingPropsSystTemps(Temps_composes.ProportionSysTemp(conll_text)))
    for f, s in extracted_tense_features:
        setattr(input, f, s)

    extracted_simple_or_composed_features = zip(simple_or_composed_features,
                            Temps_composes.ProportionTempsSimplesOuComposes(conll_text))
    for f, s in extracted_simple_or_composed_features:
        setattr(input, f, s)

    extracted_mode_proportion_features = zip(mode_proportion_features,
                            Temps_composes.ProportionModes(conll_text))
    
    for f, s in extracted_mode_proportion_features:
        setattr(input, f, s)

    extracted_flexions_features = zip(flexions_proportion_features,
                            ProportionPersonnes(conll_text) + ProportionNombre(conll_text) )
    for f, s in extracted_flexions_features:
        setattr(input, f, s)



@register_processor(processor_name)
class TensesProcessor(Processor):
    """ Processor that computes tenses proportion at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)  

        return doc
