from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

import os
import re

processor_name = 'pronoms'

features_list = [
    'repartition_pronoms_personnels_1e_personne', 
    'repartition_pronoms_personnels_2e_personne', 
    'repartition_pronoms_personnels_3e_personne',
    'repartition_pronoms_personnels_singuliers', 
    'repartition_pronoms_personnels_pluriels',
    'proportion_pronoms_personnels_1e_personne', 
    'proportion_pronoms_personnels_2e_personne', 
    'proportion_pronoms_personnels_3e_personne',    
    'proportion_pronoms_personnels',
    'proportion_pronoms_personnels_singuliers', 
    'proportion_pronoms_personnels_pluriels',
    'proportion_pronoms_relatifs',
    'proportion_pronoms_interrogatifs',
    'proportion_pronoms_demonstratifs',
    'proportion_pronoms_possessifs',
    'proportion_pronoms_negatifs',
    'nombre_pronoms_indefinis',
    'proportion_pronoms_indefinis']

def setFeatures(input):

    features_count_dict = dict.fromkeys(features_list , 0)
    proportion_features_dict = dict.fromkeys(features_list , 0)

    total_pronouns_person = 0
    total_pronouns_number = 0
    total_words_count = 0


    for word in input.words_list:
        if (word.upos == "PRON") and word.feats != None:  

            features_dictionary = dict([(e.split('=')[0], e.split('=')[1]) for e in word.feats.split('|')])

            pronoun_type = features_dictionary.get("PronType") 
            
            if pronoun_type != None:
                if pronoun_type == "Prs":
                    features_count_dict['proportion_pronoms_personnels'] += 1
                    number = features_dictionary.get("Number") 
                    person = features_dictionary.get("Person") 
                    if person != None and person != '<UNK>':
                        total_pronouns_person += 1
                        features_count_dict['repartition_pronoms_personnels_'+person+'e_personne'] += 1
                        features_count_dict['proportion_pronoms_personnels_'+person+'e_personne'] += 1
                    if number != None:
                        total_pronouns_number += 1
                        if number == "Sing":
                            features_count_dict['repartition_pronoms_personnels_singuliers'] += 1
                            features_count_dict['proportion_pronoms_personnels_singuliers'] += 1
                        elif number == "Plur":
                            features_count_dict['repartition_pronoms_personnels_pluriels'] += 1 
                            features_count_dict['proportion_pronoms_personnels_pluriels'] += 1  

                elif pronoun_type == "Rel":
                    features_count_dict['proportion_pronoms_relatifs'] += 1 
                     
                elif pronoun_type == "Int":
                    features_count_dict['proportion_pronoms_interrogatifs'] += 1
                     
                elif pronoun_type == "Dem":
                    features_count_dict['proportion_pronoms_demonstratifs'] += 1
                     
                elif pronoun_type == "Neg":
                    features_count_dict['proportion_pronoms_negatifs'] += 1                    

            #todo "pas un"
            elif word.lemma == "nul" or word.lemma == "aucun" or word.lemma == "personne" or word.lemma == "rien":
                features_count_dict['proportion_pronoms_negatifs'] += 1

        
        elif "mienne" in word.text or "tienne" in word.text or "sienne" in word.text or "nôtre" in word.text or "vôtre" in word.text :
            features_count_dict['proportion_pronoms_possessifs'] += 1
        
        if word.upos != "PUNCT":
            total_words_count += 1
            if total_pronouns_person > 0:
                proportion_features_dict['repartition_pronoms_personnels_1e_personne'] = features_count_dict['repartition_pronoms_personnels_1e_personne']/total_pronouns_person
                proportion_features_dict['repartition_pronoms_personnels_2e_personne'] = features_count_dict['repartition_pronoms_personnels_2e_personne']/total_pronouns_person
                proportion_features_dict['repartition_pronoms_personnels_3e_personne'] = features_count_dict['repartition_pronoms_personnels_3e_personne']/total_pronouns_person
            if total_pronouns_number > 0:
                proportion_features_dict['repartition_pronoms_personnels_singuliers'] = features_count_dict['repartition_pronoms_personnels_singuliers']/total_pronouns_number
                proportion_features_dict['repartition_pronoms_personnels_pluriels'] = features_count_dict['repartition_pronoms_personnels_pluriels']/total_pronouns_number

            proportion_features_dict['proportion_pronoms_personnels'] = features_count_dict['proportion_pronoms_personnels']/total_words_count
            proportion_features_dict['proportion_pronoms_personnels_1e_personne'] = features_count_dict['proportion_pronoms_personnels_1e_personne']/total_words_count
            proportion_features_dict['proportion_pronoms_personnels_2e_personne'] = features_count_dict['proportion_pronoms_personnels_2e_personne']/total_words_count
            proportion_features_dict['proportion_pronoms_personnels_3e_personne'] = features_count_dict['proportion_pronoms_personnels_3e_personne']/total_words_count
            proportion_features_dict['proportion_pronoms_personnels_singuliers'] = features_count_dict['proportion_pronoms_personnels_singuliers']/total_words_count
            proportion_features_dict['proportion_pronoms_personnels_pluriels'] = features_count_dict['proportion_pronoms_personnels_pluriels']/total_words_count
            proportion_features_dict['proportion_pronoms_relatifs'] = features_count_dict['proportion_pronoms_relatifs']/total_words_count
            proportion_features_dict['proportion_pronoms_interrogatifs'] = features_count_dict['proportion_pronoms_interrogatifs']/total_words_count
            proportion_features_dict['proportion_pronoms_demonstratifs'] = features_count_dict['proportion_pronoms_demonstratifs']/total_words_count
            proportion_features_dict['proportion_pronoms_possessifs'] = features_count_dict['proportion_pronoms_possessifs']/total_words_count
            proportion_features_dict['proportion_pronoms_negatifs'] = features_count_dict['proportion_pronoms_negatifs']/total_words_count


    # Add features for pronoms_indefinis
    setFeaturesPronomsIndefinis(input, proportion_features_dict, total_words_count)

    for fp, f in zip(features_list, list(proportion_features_dict.values())):
        setattr(input, fp, f)


# Gestion des pronoms indéfinis : à partir d'une liste de mots ou expressions

def load_pronoms_indefinis_dictionary():
    filename = 'processor/morphosyntaxe/pronoms_indefinis.txt'
    dictionary = []
    if os.path.isfile(filename):
        with open(filename, 'r', encoding="utf-8") as file:
            for line in file:  
                dictionary.append(line.rstrip())
    return dictionary

pronoms_indefinis_dictionary = load_pronoms_indefinis_dictionary()

def setFeaturesPronomsIndefinis(input, proportion_features_dict, total_words_count):
    pronoms_indefins_count = extract_PronomsIndefinis_count(input.text)
    proportion_features_dict['nombre_pronoms_indefinis'] = pronoms_indefins_count
    if (total_words_count != 0):
        proportion_features_dict['proportion_pronoms_indefinis'] = pronoms_indefins_count / total_words_count
    else:
        proportion_features_dict['proportion_pronoms_indefinis'] = 0
  
def extract_PronomsIndefinis_count(text):
    feature_count = 0
    for feature_value in pronoms_indefinis_dictionary: 
        feature_count += len(re.findall(feature_value, text))
    return feature_count


@register_processor(processor_name)
class PronounsProcessor(Processor):
    """ Processor that computes pronouns proportion at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)  

        return doc
