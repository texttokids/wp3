#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import re
from collections import defaultdict

###------INFOS------###
#Permet de récuperer les infos d'un lexique. Prend un argument le chemin du lexique, au format .tsv, avec :
# les termes dans la 1ère colonne
# les catégorie dans la 2nde colonne
#Lancement : python3 manageLexique.py lexique.tsv

###------FONCTIONS------###
# Récupération du lexique
def getLexicon(f_path):
	"""
	Entrée : chemin du fichier .tsv avec le lexique
	Sortie : le lexique, sous forme de dicos
	1. dico_cat_terms : {"catégorie" : ["terme1", "terme 2"]}
	2. dico_terms_cat : {"terme1" : "catégorie", "terme2" : "catégorie"}
	"""
	
	dico_cat_terms = defaultdict(lambda :list())
	dico_terms_cat = dict()
	
	with open(f_path,'r',encoding="utf-8") as in_stream :
		in_stream.readline() #on évacue la 1ère ligne, celle avec les en-têtes de colonne
		for line in in_stream : #pour chaque ligne du fichier
			elements = line.strip().split("\t") #on récupère le terme et la catégorie
			dico_cat_terms[elements[1]].append(elements[0]) #ajout du terme dans la liste de termes associés à une catégorie
			dico_terms_cat[elements[0]] = elements[1] #ajout du couple terme/catégorie
	
	return dico_cat_terms, dico_terms_cat

# Filtrage des termes
def get_DoubleAgents(liste_cat,dico_cat_terms):
	"""
	Entrée :
	- liste des catégories
	- dictionnaire { catégorie : [terme 1, terme 2] }
	Sortie : dictionnaire { (catégorie 1, catégorie 2) : [terme en commun 1, terme en commun 2] }
	"""
	
	agents_doubles=dict()

	for i,cat in enumerate(liste_cat): #pour chaque catégorie
		for j in range(i+1,len(liste_cat)): #on va chercher les catégories suivantes
			autre_cat = liste_cat[j]
			#on compare les catégories deux à deux en faisant l'intersection des termes qui leur sont associés
			commun = set(dico_cat_terms[cat])&set(dico_cat_terms[autre_cat])
			if commun : #si des termes en commun on été trouvés
				agents_doubles[(cat,autre_cat)]=list(commun) #on rentre le couple de catégories et les termes en commun dans le dico

	return agents_doubles
	
def suppr_terms(agents_doubles,dico_cat_terms):
	"""
	Entrée :
	- dictionnaire { (catégorie 1, catégorie 2) : [terme en commun 1, terme en commun 2] }
	- dictionnaire { catégorie : [terme 2, terme 2]}
	Sortie : un message pour indiquer quels termes ont été supprimés de quelles catégories
	"""
	
	affichage = "/!\Supression de termes /!\\"
	
	for couple in agents_doubles : #pour chaque couple de catégories ayant des termes en commun
		affichage += "\nCatégories '"+couple[0]+"' et '"+couple[1]+"' :"
		for conflit in agents_doubles[couple]: #pour chaque terme en commun
			affichage += "\n-"+conflit
			dico_cat_terms[couple[0]].remove(conflit) #suppression pour la 1ère cat
			dico_cat_terms[couple[1]].remove(conflit) #suppression pour la 2nde
	
	return affichage	
	
###------EXECUTION------###
if __name__ == '__main__' :

	##ETAPE 1 : récupérer le lexique

	#f_path = "../Lexiques/lexique_emotionnel.tsv" #chemin du fichier .tsv avec le lexique
	f_path = sys.argv[1]
	dico_cat_lemmes, dico_lemmes_cat = getLexicon(f_path) #récupération du lexique
	
	# Quelques données sur le lexique
	liste_cat = list(dico_cat_lemmes.keys()) #liste des catégories
	liste_lemmes = list(dico_lemmes_cat.keys()) #liste des termes

	#Vérificiation des termes en commun
	agents_doubles = get_DoubleAgents(liste_cat,dico_cat_lemmes)
	if agents_doubles :
		message = suppr_terms(agents_doubles,dico_cat_lemmes)
		print(message)
	
	##ETAPE 2 : appliquer le lexique
	#Il suffit de chercher les mots d'un texte dans le lexique
	test = "Ce mouton est très heureux."

	mot = re.compile("(\w+)") #regEx pour trouver tous les mots
	resultat = list()
	for match in re.finditer(mot, test): #pour chaque correspondace
		m = match.group(1) #le mot
		if m in dico_lemmes_cat :
			m = "<cat ='"+dico_lemmes_cat[m]+"'>"+m+"</cat>"
		resultat.append(m)
	
	print(" ".join(resultat))
	