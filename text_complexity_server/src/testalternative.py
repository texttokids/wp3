# testchain.py
# direct call of texttokids main chain
import os
import sys

def set_local_path():
    localcurdir = os.getcwd()
    print("the current directory", localcurdir)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("path to testalternative.py", dir_path)
    os.chdir(dir_path)
    # sys.path.insert(1, dir_path)
    return localcurdir

def read_whole_file(name):
    # Open a file: file
    file = open(name, mode='r')
    # read all lines at once
    all_of_it = file.read()
    # close the file
    file.close()
    return all_of_it

def printto(fname, content):
    f = open(fname, 'w')
    print(content, file=f)
    f.close()
    return fname

if __name__ == "__main__":
    locpth = set_local_path()
    sys.path.insert(1, "../alternative/rashedur/")
    from text_processor import alternative
    f_input_dir = locpth + "/"
    f_output_dir = locpth + "/"

    if len(sys.argv) != 4:
        print('usage: testalternative.py text processor level')
        sys.exit(1)
    if sys.argv[1][0] == "@":
        filename = sys.argv[1][1:]
    else:
        filename = printto("myinput.txt", sys.argv[1])
    mylevel = sys.argv[2].split(",")
    myproc = sys.argv[3].split(",")
    alt_path = "../alternative/rashedur/"
    new_name = filename.replace(".txt", ".zip")
    res = alternative(alt_path, f_input_dir, f_output_dir, filename, new_name, mylevel, myproc)
    print(res)
    os.chdir(locpth)
